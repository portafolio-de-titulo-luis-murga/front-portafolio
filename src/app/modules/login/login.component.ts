import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { filter, finalize, take } from 'rxjs/operators';

import { LoginService } from 'src/app/services/api/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  public currentDate: Date = new Date();
  public loginForm: FormGroup;
  public errorLogin: boolean = false;
  public isloading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
    this.loginService.logOut();
    this.initStyleClass();
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      user: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  toLowercase(event: any): void {
    event.target.value = event.target.value.toLowerCase();
  }

  login(): void {
    if (this.loginForm.valid) {
      this.isloading = true;

      const credentials = {
        username: this.loginForm.value['user'].toLowerCase(),
        pwd: this.loginForm.value['password']
      };
      this.errorLogin = false;

      this.loginService.login(credentials).pipe(
        take(1),
        filter(isError => !isError),
        finalize(() => this.isloading = false)
      ).subscribe(() => this.errorLogin = true);
    }
  }

  initStyleClass(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');
    body.classList.remove('nav-open');
    const card = document.getElementById('loginCard');
    setTimeout(() => card.classList.remove('card-hidden'), 400);
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

}
