import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChangePwdComponent } from 'src/app/shared/components/change-pwd/change-pwd.component';
import { GraphicsComponent } from 'src/app/shared/components/graphics/graphics.component';
import { ResourceCountResolver } from 'src/app/shared/resolver/resource-count.resolver';


const routes: Routes = [
  {
    path: 'home',
    component: GraphicsComponent,
    resolve: { rslv: ResourceCountResolver }
  },
  {
    path: 'cambiarClave',
    component: ChangePwdComponent
  },
  {
    path: 'provider',
    loadChildren: () => import('./pages/providers/providers.module').then(m => m.ProvidersModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'unit',
    loadChildren: () => import('./pages/units/units.module').then(m => m.UnitsModule)
  },
  {
    path: 'anexo',
    loadChildren: () => import('./pages/anexos/anexos.module').then(m => m.AnexosModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./pages/reports/reports.module').then(m => m.ReportsModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
