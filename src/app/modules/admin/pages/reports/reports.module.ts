import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';

import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GeneralReportComponent } from './components/general-report/general-report.component';
import { TarificacionAnexoUnitComponent } from './components/tarificacion-anexo-unit/tarificacion-anexo-unit.component';
import { ReportAnexoUnitComponent } from './components/tarificacion-anexo-unit/components/report-anexo-unit/report-anexo-unit.component';
import { SupervisorReportComponent } from './components/supervisor-report/supervisor-report.component';


@NgModule({
  declarations: [
    GeneralReportComponent,
    TarificacionAnexoUnitComponent,
    ReportAnexoUnitComponent,
    SupervisorReportComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReportsModule { }
