import { TestBed } from '@angular/core/testing';

import { GetReportUniversityResolver } from './get-report-university.resolver';

describe('GetReportUniversityResolver', () => {
  let resolver: GetReportUniversityResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetReportUniversityResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
