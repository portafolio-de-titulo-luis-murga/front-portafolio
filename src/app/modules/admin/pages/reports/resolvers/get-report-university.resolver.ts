import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { FacultyService } from 'src/app/services/api/faculty.service';
import { DayJS } from 'src/app/shared/utils/date-util';

@Injectable({
  providedIn: 'root'
})
export class GetReportUniversityResolver implements Resolve<boolean> {

  constructor(
    private facultyService: FacultyService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.spinner.show();
    const currentDate = DayJS().format('MM/YYYY');
    return this.facultyService.getUniversityReport(currentDate).pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
