import { TestBed } from '@angular/core/testing';

import { GetSupervisorsResolver } from './get-supervisors.resolver';

describe('GetSupervisorsResolver', () => {
  let resolver: GetSupervisorsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetSupervisorsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
