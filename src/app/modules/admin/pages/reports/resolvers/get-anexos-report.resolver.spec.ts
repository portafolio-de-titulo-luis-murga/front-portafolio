import { TestBed } from '@angular/core/testing';

import { GetAnexosReportResolver } from './get-anexos-report.resolver';

describe('GetAnexosReportResolver', () => {
  let resolver: GetAnexosReportResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetAnexosReportResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
