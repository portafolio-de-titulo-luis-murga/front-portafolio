import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { DayJS } from 'src/app/shared/utils/date-util';
import { AnexoService } from 'src/app/services/api/anexo.service';

@Injectable({
  providedIn: 'root'
})
export class GetAnexosReportResolver implements Resolve<boolean> {

  constructor(
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const maxDate = DayJS();
    const minDate: Date = maxDate.subtract(5, 'year').set('month', 0).toDate();

    this.spinner.show();
    return this.anexoService.getAnexosReport(maxDate.format('YYYY-MM-DD'), maxDate.format('YYYY-MM-DD')).pipe(
      map(resp => ({
        anexos: resp,
        minDate,
        maxDate: maxDate.toDate()
      })),
      finalize(() => this.spinner.hide())
    );
  }
}
