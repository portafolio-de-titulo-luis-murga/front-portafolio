import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subject, Subscription } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

import { Moment } from 'moment';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';

import { DayJS } from 'src/app/shared/utils/date-util';
import { ReportService } from 'src/app/services/api/report.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';
import { FacultyService } from 'src/app/services/api/faculty.service';



const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-general-report',
  templateUrl: './general-report.component.html',
  styleUrls: ['./general-report.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class GeneralReportComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  public date = new FormControl(moment(), Validators.required);

  public minDate: Date;
  public maxDate: Date;

  public divHaveClass: boolean = true;
  public isXsView: boolean = false;
  public loadingDataTable: boolean = true;

  public selectedFilter: number = 0;
  public facultiesReport: any[] = [];
  public allFacultiesReport: any[] = [];
  public totalCall: number;
  public totalSpoken: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private facultyService: FacultyService,
    private spinner: NgxSpinnerService,
    private reportService: ReportService,
    private sweetAlertService: SweetAlertService
  ) {
    this.maxDate = DayJS().toDate();
    this.minDate = DayJS().subtract(5, 'year').set('month', 0).toDate();
  }

  ngOnInit(): void {
    this.facultiesReport = this.activatedRoute.snapshot.data['rslv']['facultiesReport'];
    this.allFacultiesReport = this.activatedRoute.snapshot.data['rslv']['facultiesReport'];
    this.totalCall = this.activatedRoute.snapshot.data['rslv']['totalCalls'];
    this.totalSpoken = this.activatedRoute.snapshot.data['rslv']['totalSpokenMin'];
    this.observerResolution();
  }

  observerResolution(): void {
    // Escucha cuando la resolución es diferente a la más baja
    const suscript = this.breakpointObserver.observe(
      [Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small]
    ).pipe(takeUntil(this.unsubscribe$)).subscribe(result => {
      this.divHaveClass = result.matches;
    });
    this.subscription.add(suscript);
    this.divHaveClass = this.breakpointObserver.isMatched([
      Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small
    ]);

    // Escucha cuando la resolución es la más baja
    const suscript2 = this.breakpointObserver.observe(Breakpoints.XSmall).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(result => {
      this.isXsView = result.matches;
    });
    this.subscription.add(suscript2);
    this.isXsView = this.breakpointObserver.isMatched(Breakpoints.XSmall);
  }

  setMonthAndYear(normalizedMonthAndYear: Moment, datepicker: MatDatepicker<Moment>): void {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonthAndYear.month());
    ctrlValue.year(normalizedMonthAndYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  changeSelect(idFaculty: number): void {
    const selectedIdFaculty = Number(idFaculty);

    if (selectedIdFaculty !== 0) {
      this.facultiesReport = this.allFacultiesReport.filter(faculty => faculty.idFaculty === selectedIdFaculty);
    } else {
      this.facultiesReport = [...this.allFacultiesReport];
    }
  }

  filter(): void {
    if (this.date.valid) {
      this.spinner.show();

      const dateFilter = this.date.value.format("MM/YYYY");

      this.facultyService.getUniversityReport(dateFilter).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(dataUniversity => {
        this.selectedFilter = 0;
        this.facultiesReport = dataUniversity.facultiesReport;
        this.allFacultiesReport = dataUniversity.facultiesReport;
        this.totalCall = dataUniversity.totalCalls;
        this.totalSpoken = dataUniversity.totalSpokenMin;
      });
    }
  }

  downLoadPdf(dataPdf: any): void {
    const linkSource = `data:application/pdf;base64,${ dataPdf.pdfFile }`;
    const downloadLink = document.createElement("a");
    const fileName = `${dataPdf.fileName}.pdf`;

    downloadLink.href = linkSource;
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  exportPdf(): void {
    this.spinner.show();

    const data = {
      dateMonthYear: this.date.value.format("MM/YYYY")
    };

    this.reportService.generatePdf('RPUNIVERSITY', data).pipe(
      take(1),
      finalize(() => this.spinner.hide())
    ).subscribe(data => {
      this.downLoadPdf(data);
      this.sweetAlertService.sweetModal({
        title: 'Generación PDF',
        text: 'Hemos enviado una notificación a tu correo con el archivo adjunto',
        buttonStyle: 'btn btn-success',
        confirmButtonText: 'Cerrar',
        icon: 'success'
      });
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
