import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

import { Moment } from 'moment';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { DayJS } from 'src/app/shared/utils/date-util';
import { ActivatedRoute } from '@angular/router';
import { ExcelService } from 'src/app/services/common/excel.service';
import { ReportService } from 'src/app/services/api/report.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-supervisor-report',
  templateUrl: './supervisor-report.component.html',
  styleUrls: ['./supervisor-report.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class SupervisorReportComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  public date = new FormControl(moment(), Validators.required);

  public minDate: Date;
  public maxDate: Date;

  public divHaveClass: boolean = true;
  public isXsView: boolean = false;
  public loadingDataTable: boolean = true;

  public supervisorList: any[] = [];
  public selectedFilter: any;
  public dataSupervisor: any;

  public messageAlert: string = 'Debe realizar un filtro para ver información.';

  constructor(
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private reportService: ReportService,
    private sweetAlertService: SweetAlertService
  ) {
    this.maxDate = DayJS().toDate();
    this.minDate = DayJS().subtract(5, 'year').set('month', 0).toDate();
  }

  ngOnInit(): void {
    this.supervisorList = this.activatedRoute.snapshot.data['rslv'];
    this.observerResolution();
  }

  observerResolution(): void {
    // Escucha cuando la resolución es diferente a la más baja
    const suscript = this.breakpointObserver.observe(
      [Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small]
    ).pipe(takeUntil(this.unsubscribe$)).subscribe(result => {
      this.divHaveClass = result.matches;
    });
    this.subscription.add(suscript);
    this.divHaveClass = this.breakpointObserver.isMatched([
      Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small
    ]);

    // Escucha cuando la resolución es la más baja
    const suscript2 = this.breakpointObserver.observe(Breakpoints.XSmall).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(result => {
      this.isXsView = result.matches;
    });
    this.subscription.add(suscript2);
    this.isXsView = this.breakpointObserver.isMatched(Breakpoints.XSmall);
  }

  setMonthAndYear(normalizedMonthAndYear: Moment, datepicker: MatDatepicker<Moment>): void {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonthAndYear.month());
    ctrlValue.year(normalizedMonthAndYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  filter(): void {
    if (this.date.valid && this.selectedFilter) {
      this.spinner.show();

      const dateFilter = this.date.value.format("MM/YYYY");
      const idUser = this.selectedFilter.idUser;

      this.reportService.getSupervisorTarification(dateFilter, idUser).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(dataSupervisor => {
        if (dataSupervisor.code === '000') {
          this.dataSupervisor = dataSupervisor.data;
        } else if (dataSupervisor.code === '001') {
          this.messageAlert = 'No hay datos para el responsable seleccionado.';
          this.dataSupervisor = null;
        }
      });
    }

  }

  downLoadPdf(dataPdf: any): void {
    const linkSource = `data:application/pdf;base64,${ dataPdf.pdfFile }`;
    const downloadLink = document.createElement("a");
    const fileName = `${dataPdf.fileName}.pdf`;

    downloadLink.href = linkSource;
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  exportPdf(): void {
    if (this.selectedFilter && this.dataSupervisor) {
      this.spinner.show();

      const data = {
        dateMonthYear: this.date.value.format("MM/YYYY"),
        idUserSupervisor: this.selectedFilter.idUser
      };

      this.reportService.generatePdf('RPRESPONSABLE', data).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(data => {
        this.downLoadPdf(data);
        this.sweetAlertService.sweetModal({
          title: 'Generación PDF',
          text: 'Hemos enviado una notificación a tu correo con el archivo adjunto',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Cerrar',
          icon: 'success'
        });
      });
    }
  }

  exportExcel(): void {
    if (this.selectedFilter && this.dataSupervisor) {
      this.spinner.show();

      const dateFilter = this.date.value.format("MM/YYYY");
      const idUser = this.selectedFilter.idUser;

      this.reportService.getSupervisorExcel(dateFilter, idUser).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(dataForExcel => {
        const headers = ['Responsable', 'Unidad', 'anexo',
        'SLM (Segundos)', 'SLM ($)', 'SLM (Cant. Llamadas)',
        'CEL (Segundos)', 'CEL ($)', 'CEL (Cant. Llamadas)',
        'LDI (Segundos)', 'LDI ($)', 'LDI (Cant. Llamadas)'];

        const dateTime = new Date().getTime();
        const fileName = `responsable-${dateTime}`;

        this.excelService.exportToExcel(dataForExcel, fileName, headers);
      });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
