import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarificacionAnexoUnitComponent } from './tarificacion-anexo-unit.component';

describe('TarificacionAnexoUnitComponent', () => {
  let component: TarificacionAnexoUnitComponent;
  let fixture: ComponentFixture<TarificacionAnexoUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarificacionAnexoUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarificacionAnexoUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
