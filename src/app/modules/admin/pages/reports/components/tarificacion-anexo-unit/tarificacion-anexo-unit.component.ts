import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { finalize, take } from 'rxjs/operators';

import { AnexoService } from 'src/app/services/api/anexo.service';
import { UnitService } from 'src/app/services/api/unit.service';
import { DayJS } from 'src/app/shared/utils/date-util';

@Component({
  selector: 'app-tarificacion-anexo-unit',
  templateUrl: './tarificacion-anexo-unit.component.html',
  styleUrls: ['./tarificacion-anexo-unit.component.css']
})
export class TarificacionAnexoUnitComponent implements OnInit {

  public view: string = 'loading';
  public tabPane: string = 'link7';

  public dataTable: any[];
  public minDatePicker: Date;
  public maxDatePicker: Date;

  constructor(
    private activatedRoute: ActivatedRoute,
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService,
    private unitService: UnitService
  ) {}

  ngOnInit(): void {
    this.initView();
  }

  initView(): void {
    this.dataTable = this.activatedRoute.snapshot.data['rslv']['anexos'];
    this.minDatePicker = this.activatedRoute.snapshot.data['rslv']['minDate'];
    this.maxDatePicker = this.activatedRoute.snapshot.data['rslv']['maxDate'];
    this.view = 'anexos';
  }

  changeView(typeView: string): void {
    if (typeView !== this.view) {
      this.view = 'loading';
      this.spinner.show();

      const lastDate = DayJS();
      this.maxDatePicker = lastDate.toDate();

      if (typeView === 'anexos') {
        this.anexoService.getAnexosReport(lastDate.format('YYYY-MM-DD'), lastDate.format('YYYY-MM-DD')).pipe(
          take(1),
          finalize(() => this.spinner.hide())
        ).subscribe((dataForTable: any[]) => {
          this.dataTable = dataForTable;
          this.tabPane = 'link7';
          this.view = 'anexos';
        });
      } else if (typeView === 'unidades') {
        this.unitService.getUnitsReport(lastDate.format('YYYY-MM-DD'), lastDate.format('YYYY-MM-DD')).pipe(
          take(1),
          finalize(() => this.spinner.hide())
        ).subscribe((dataForTable: any[]) => {
          this.dataTable = dataForTable;
          this.tabPane = 'link8';
          this.view = 'unidades';
        });
      }
    }
  }

}
