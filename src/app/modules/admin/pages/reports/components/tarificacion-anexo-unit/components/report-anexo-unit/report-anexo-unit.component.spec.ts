import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAnexoUnitComponent } from './report-anexo-unit.component';

describe('ReportAnexoUnitComponent', () => {
  let component: ReportAnexoUnitComponent;
  let fixture: ComponentFixture<ReportAnexoUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportAnexoUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAnexoUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
