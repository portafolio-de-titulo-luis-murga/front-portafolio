import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { AnexoService } from 'src/app/services/api/anexo.service';
import { ExcelService } from 'src/app/services/common/excel.service';
import { ReportService } from 'src/app/services/api/report.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';
import { DayJS } from 'src/app/shared/utils/date-util';
import { UnitService } from 'src/app/services/api/unit.service';

declare const $: any;

@Component({
  selector: 'app-report-anexo-unit',
  templateUrl: './report-anexo-unit.component.html',
  styleUrls: ['./report-anexo-unit.component.css']
})
export class ReportAnexoUnitComponent implements OnInit, AfterViewInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  @Input('typeTable') typeTable: string;
  @Input('dataTable') dataTable: any[] = [];
  @Input('minDate') minDate: Date;
  @Input('maxDate') maxDate: Date;
  @Input('initDatePicker') initDatePicker: Date;

  headersTable: string[] = [];

  rangeDate: FormGroup;

  divHaveClass: boolean = true;
  isXsView: boolean = false;
  loadingDataTable: boolean = true;

  constructor(
    private fb: FormBuilder,
    private breakpointObserver: BreakpointObserver,
    private excelService: ExcelService,
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService,
    private reportService: ReportService,
    private sweetAlertService: SweetAlertService,
    private unitService: UnitService
  ) { }

  ngOnInit(): void {
    this.observerResolution();
    this.initFormDatepicker();
    this.initDataTable();
  }

  loadTable(): void {
    this.loadingDataTable = false;
    setTimeout(() => {
      // Cargando tabla
      $(`#${this.typeTable}`).DataTable({
        pagingType: "full_numbers",
        ordering: false,
        oLanguage: {
          sLengthMenu: "Mostrar _MENU_ registros",
          sEmptyTable: "Sin datos disponibles",
          sInfoEmpty: "Sin resgistros para mostrar",
          sLoadingRecords: "Cargando...",
          sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
          sSearch: `${this.typeTable === 'anexos' ? 'Buscar Anexo:' : 'Buscar Unidad:'}`,
          sInfoFiltered: "(filtrado de _MAX_ registros en total)",
          sZeroRecords: "No se encontraron registros coincidentes",
          oPaginate: {
            sFirst: "Primero",
            sLast: "Último",
            sNext: "Siguiente",
            sPrevious: "Anterior"
          }
        },
        columnDefs: [
          { searchable: false, targets: [1,2,3,4,5,6,7,8] },
          // { width: '250px', targets: [0,1,2] },
          // { width: '150px', targets: [3,4,5,6,7] },
          // { width: '100px', targets: [8] },
        ],
        lengthMenu: [
          [5, 10, 25, 50, -1],
          [5, 10, 25, 50, "Todo"]
        ],
        // responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
        }

      });

      $('.card .material-datatables label').addClass('form-group');
    });
  }

  ngAfterViewInit(): void {
    this.loadTable();
  }

  initDataTable(): void {
    if (this.typeTable === 'anexos') {
      this.headersTable = ['Anexo', 'SLM (seg)', 'SLM ($)', 'CEL (seg)', 'CEL ($)', 'LDI (seg)', 'LDI ($)', 'Cant. Llamadas'];
    } else if (this.typeTable === 'unidades') {
      this.headersTable = ['Unidad', 'SLM (seg)', 'SLM ($)', 'CEL (seg)', 'CEL ($)', 'LDI (seg)', 'LDI ($)', 'Cant. Llamadas'];
    }
  }

  initFormDatepicker(): void {
    this.rangeDate = this.fb.group({
      start: [this.initDatePicker, Validators.required],
      end: [this.maxDate, Validators.required],
    });
  }

  exportExcel(): void {
    let headers = [];
    let dataForExcel = [];
    if (this.typeTable === 'anexos') {
      headers = ['Anexo', 'Unidad', 'Facultad', 'Proveedor', 'CEL (Segundos)', 'CEL ($)', 'CEL (Cant. Llamadas)', 'SLM (Segundos)', 'SLM ($)', 'SLM (Cant. Llamadas)', 'LDI (Segundos)', 'LDI ($)', 'LDI (Cant. Llamadas)', 'Total Llamadas (Segundos)', 'Costo Total', 'Cant. Total Llamadas'];

      dataForExcel = [...this.dataTable];
    } else if (this.typeTable === 'unidades') {
      headers = ['Unidad', 'Responsable', 'Facultad', 'Proveedor', 'Cant. Anexos', 'Costo del Anexo',
      'SLM (Segundos)', 'SLM ($)', 'SLM (Cant. Llamadas)', 'Costo/Seg SLM',
      'CEL (Segundos)', 'CEL ($)', 'CEL (Cant. Llamadas)', 'Costo/Seg CEL',
      'LDI (Segundos)', 'LDI ($)', 'LDI (Cant. Llamadas)', 'Costo/Seg LDI',
      'Total Llamadas (Segundos)', 'Cant. Total Llamadas', 'Costo Total'];

      dataForExcel = this.dataTable.map(data => ({
        unitName: data.unitName,
        userName: data.userName,
        facultyName: data.facultyName,
        nameProvider: data.nameProvider,
        countAnexos: data.countAnexos,
        fixedCostAnexos: data.fixedCostAnexos,
        slmTotalSeg: data.slmTotalSeg,
        slmTotalCost: data.slmTotalCost,
        slmCallQuantity: data.slmCallQuantity,
        slmCostService: data.slmCostService,
        celTotalSeg: data.celTotalSeg,
        celTotalCost: data.celTotalCost,
        celCallQuantity: data.celCallQuantity,
        celCostService: data.celCostService,
        ldiTotalSeg: data.ldiTotalSeg,
        ldiTotalCost: data.ldiTotalCost,
        ldiCallQuantity: data.ldiCallQuantity,
        ldiCostService: data.ldiCostService,
        totalSeg: data.totalSeg,
        callQuantity: data.callQuantity,
        totalCost: data.totalCost
      }));
    }

    const dateTime = new Date().getTime();
    const fileName = `${this.typeTable}-${dateTime}`;

    this.excelService.exportToExcel(dataForExcel, fileName, headers);
  }

  observerResolution(): void {
    // Escucha cuando la resolución es diferente a la más baja
    const suscript = this.breakpointObserver.observe(
      [Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small]
    ).pipe(takeUntil(this.unsubscribe$)).subscribe(result => {
      this.divHaveClass = result.matches;
    });
    this.subscription.add(suscript);
    this.divHaveClass = this.breakpointObserver.isMatched([
      Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small
    ]);

    // Escucha cuando la resolución es la más baja
    const suscript2 = this.breakpointObserver.observe(Breakpoints.XSmall).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(result => {
      this.isXsView = result.matches;
    });
    this.subscription.add(suscript2);
    this.isXsView = this.breakpointObserver.isMatched(Breakpoints.XSmall);
  }

  filter(): void {
    if (this.rangeDate.valid) {
      this.spinner.show();
      const initDate = DayJS(this.rangeDate.value['start']).format('YYYY-MM-DD');
      const lastDate = DayJS(this.rangeDate.value['end']).format('YYYY-MM-DD');

      this.loadingDataTable = true;
      if (this.typeTable === 'anexos') {
        this.anexoService.getAnexosReport(initDate, lastDate).pipe(
          take(1),
          finalize(() => this.spinner.hide())
        ).subscribe((dataForTable: any[]) => {
          this.dataTable = dataForTable;
          this.loadTable();
        });
      } else {
        this.unitService.getUnitsReport(initDate, lastDate).pipe(
          take(1),
          finalize(() => this.spinner.hide())
        ).subscribe((dataForTable: any[]) => {
          this.dataTable = dataForTable;
          this.loadTable();
        });
      }

    }
  }

  generatePdf(idTemplate: string, data: any, sendEmail: boolean): void {
    this.spinner.show();
    this.reportService.generatePdf(idTemplate, data).pipe(
      take(1),
      finalize(() => this.spinner.hide())
    ).subscribe(data => {
      this.downLoadPdf(data);
      if (sendEmail) {
        this.sweetAlertService.sweetModal({
          title: 'Generación PDF',
          text: 'Hemos enviado una notificación a tu correo con el archivo adjunto',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Cerrar',
          icon: 'success'
        });
      }
    });
  }

  downLoadPdf(dataPdf: any): void {
    const linkSource = `data:application/pdf;base64,${ dataPdf.pdfFile }`;
    const downloadLink = document.createElement("a");
    const fileName = `${dataPdf.fileName}.pdf`;

    downloadLink.href = linkSource;
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  exportAllPdf(): void {
    const initDate = DayJS(this.rangeDate.value['start']).format('YYYY-MM-DD');
    const lastDate = DayJS(this.rangeDate.value['end']).format('YYYY-MM-DD');
    const idTemplate = this.typeTable === 'anexos' ? 'RPALLANEXOS' : 'RPALLUNITS';
    const data = { initDate, lastDate };
    this.generatePdf(idTemplate, data, true);
  }

  exportPdf(idElement: number): void {
    let idTemplate: string;
    let data: any;
    const initDate = DayJS(this.rangeDate.value['start']).format('YYYY-MM-DD');
    const lastDate = DayJS(this.rangeDate.value['end']).format('YYYY-MM-DD');
    if (this.typeTable === 'anexos') {
      idTemplate = 'RPANEXO';
      data = {
        initDate,
        lastDate,
        idAnexo: idElement
      };
    } else {
      idTemplate = 'RPUNIT';
      data = {
        initDate,
        lastDate,
        idUnit: idElement
      };
    }
    this.generatePdf(idTemplate, data, false);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
