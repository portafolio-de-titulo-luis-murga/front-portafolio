import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleAdminGuard } from 'src/app/guards/role-admin.guard';
import { GeneralReportComponent } from './components/general-report/general-report.component';
import { TarificacionAnexoUnitComponent } from './components/tarificacion-anexo-unit/tarificacion-anexo-unit.component';
import { SupervisorReportComponent } from './components/supervisor-report/supervisor-report.component';
import { GetAnexosReportResolver } from './resolvers/get-anexos-report.resolver';
import { GetSupervisorsResolver } from './resolvers/get-supervisors.resolver';
import { GetReportUniversityResolver } from './resolvers/get-report-university.resolver';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [RoleAdminGuard],
    children: [
      {
        path: 'general',
        component: GeneralReportComponent,
        resolve: { rslv: GetReportUniversityResolver }
      },
      {
        path: 'anexo-unidad',
        component: TarificacionAnexoUnitComponent,
        resolve: { rslv: GetAnexosReportResolver }
      },
      {
        path: 'responsable',
        component: SupervisorReportComponent,
        resolve: { rslv: GetSupervisorsResolver }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
