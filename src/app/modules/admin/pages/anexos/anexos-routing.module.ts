import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleAdminGuard } from 'src/app/guards/role-admin.guard';
import { GetAnexosResolver } from './resolvers/get-anexos.resolver';
import { GetUnitsResolver } from 'src/app/shared/resolver/get-units.resolver';
import { AnexoListComponent } from './components/anexo-list/anexo-list.component';
import { AnexoCreateComponent } from './components/anexo-create/anexo-create.component';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [RoleAdminGuard],
    children: [
      {
        path: '',
        component: AnexoListComponent,
        resolve: { rslv: GetAnexosResolver }
      },
      {
        path: 'create',
        component: AnexoCreateComponent,
        resolve: { rslv: GetUnitsResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnexosRoutingModule { }
