import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs';

import { NgxSpinnerService } from 'ngx-spinner';

import { UnitService } from 'src/app/services/api/unit.service';

@Component({
  selector: 'app-update-anexo-dialog',
  templateUrl: './update-anexo-dialog.component.html',
  styleUrls: ['./update-anexo-dialog.component.css']
})
export class UpdateAnexoDialogComponent implements OnInit {

  public unitSelected: FormControl;
  public units: any[] = [];
  public isLoading: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<UpdateAnexoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private unitService: UnitService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.unitService.getUnits().pipe(
      finalize(() => {
        this.spinner.hide();
        this.isLoading = false;
      })
    ).subscribe(units => {
      this.units = units;
      this.unitSelected = new FormControl(units[0]?.idUnit, Validators.required);
    });
  }

  onClose(): void {
    this.dialogRef.close({ resp: false });
  }

  onConfirm(): void {
    if (this.unitSelected.valid) {
      this.dialogRef.close({ resp: true, idUnit: this.unitSelected.value });
    }
  }

}
