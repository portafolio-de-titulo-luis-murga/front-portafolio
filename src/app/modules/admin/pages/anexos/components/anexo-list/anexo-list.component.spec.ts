import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnexoListComponent } from './anexo-list.component';

describe('AnexoListComponent', () => {
  let component: AnexoListComponent;
  let fixture: ComponentFixture<AnexoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnexoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnexoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
