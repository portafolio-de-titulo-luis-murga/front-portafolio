import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAnexoDialogComponent } from './update-anexo-dialog.component';

describe('UpdateAnexoDialogComponent', () => {
  let component: UpdateAnexoDialogComponent;
  let fixture: ComponentFixture<UpdateAnexoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateAnexoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAnexoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
