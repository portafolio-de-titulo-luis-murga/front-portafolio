import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { filter, finalize, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { AnexoService } from 'src/app/services/api/anexo.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';
import { UpdateAnexoDialogComponent } from './dialogs/update-anexo-dialog/update-anexo-dialog.component';

@Component({
  selector: 'app-anexo-list',
  templateUrl: './anexo-list.component.html',
  styleUrls: ['./anexo-list.component.css']
})
export class AnexoListComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  public filterAnexos: FormControl = new FormControl('', Validators.pattern('^[0-9]+$'));

  public anexos: any[] = [];
  public allAnexos: any[] = [];
  public status: any[] = [
    {
      code: 0,
      status: 'Encendido'
    },
    {
      code: 1,
      status: 'Apagado'
    }
  ];
  public statusSelectedFilter: number = 2;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private anexoService: AnexoService,
    private swaService: SweetAlertService
  ) { }

  ngOnInit(): void {
    this.anexos = this.activatedRoute.snapshot.data['rslv'];
    this.allAnexos = this.activatedRoute.snapshot.data['rslv'];

    this.observerFilterAnexos();
  }

  observerFilterAnexos(): void {
    const suscrip = this.filterAnexos.valueChanges.pipe(
      takeUntil(this.unsubscribe$),
    ).subscribe(data => {
      this.statusSelectedFilter = 2;
      if (!data) {
        this.anexos = [...this.allAnexos];
      } else {
        this.anexos = this.allAnexos.filter(anexo => anexo.idAnexo.toString().includes(data.trim()));
      }
    });
    this.subscription.add(suscrip);
  }

  goCreateAnexo(): void {
    this.router.navigate(['anexo', 'create']);
  }

  changeStatus(idAnexo: number): void {
    this.spinner.show();
    this.anexoService.changeStatus(idAnexo).pipe(
      take(1),
      finalize(() => this.spinner.hide())
    ).subscribe(anexos => {
      this.anexos = anexos;
      this.allAnexos = anexos;
      this.statusSelectedFilter = 2;
      this.filterAnexos.reset('');
    });
  }

  changeSelect(codeStatus: number): void {
    this.filterAnexos.reset('');
    this.statusSelectedFilter = codeStatus;
    if (codeStatus !== 2) {
      this.anexos = this.allAnexos.filter(anexo => anexo.isDeleted === codeStatus);
    } else {
      this.anexos = [...this.allAnexos];
    }
  }

  goUpdate(idAnexo: number): void {
    const dialogResp = this.dialog.open(UpdateAnexoDialogComponent, {
      maxWidth: '1000px',
      minWidth: '400px',
      data: { idAnexo }
    });

    dialogResp.afterClosed().pipe(
      filter(data => !!data?.resp),
      tap(() => this.spinner.show()),
      take(1),
      map(data => data.idUnit),
      switchMap((idUnit) => {
        const body = { idAnexo, idUnit };
        return this.anexoService.updateAnexo(body);
      }),
      finalize(() => this.spinner.hide())
    ).subscribe((anexos) => {
      this.anexos = anexos;
      this.allAnexos = anexos;
      this.statusSelectedFilter = 2;
      this.filterAnexos.reset('');

      this.swaService.sweetModal({
        title: 'Anexo Actualizado',
        text: '',
        buttonStyle: 'btn btn-success',
        confirmButtonText: 'Aceptar',
        icon: 'success'
      });
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
