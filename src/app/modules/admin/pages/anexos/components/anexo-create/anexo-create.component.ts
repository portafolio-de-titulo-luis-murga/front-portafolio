import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { filter, finalize, take } from 'rxjs/operators';

import { AnexoService } from 'src/app/services/api/anexo.service';

@Component({
  selector: 'app-anexo-create',
  templateUrl: './anexo-create.component.html',
  styleUrls: ['./anexo-create.component.css']
})
export class AnexoCreateComponent implements OnInit {

  public units: any[] = [];

  public formAnexo: FormGroup;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.units = this.activatedRoute.snapshot.data['rslv'];
  }

  initForm(): void {
    this.formAnexo = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[0-9]+$'), Validators.minLength(1)]],
      unit: ['', [Validators.required]]
    });
  }

  createAnexo(): void {
    if (this.formAnexo.valid) {
      this.spinner.show();

      const body = {
        idAnexo: this.formAnexo.value['name'],
        idUnit: Number(this.formAnexo.value['unit']),
      };

      this.anexoService.createAnexo(body).pipe(
        take(1),
        filter(isOk => isOk),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {
        this.formAnexo.reset();
      });
    }
  }

}
