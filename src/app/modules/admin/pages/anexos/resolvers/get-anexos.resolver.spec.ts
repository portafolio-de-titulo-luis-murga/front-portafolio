import { TestBed } from '@angular/core/testing';

import { GetAnexosResolver } from './get-anexos.resolver';

describe('GetAnexosResolver', () => {
  let resolver: GetAnexosResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetAnexosResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
