import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { AnexoService } from 'src/app/services/api/anexo.service';

@Injectable({
  providedIn: 'root'
})
export class GetAnexosResolver implements Resolve<boolean> {

  constructor(
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.anexoService.getAnexos().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
