import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AnexosRoutingModule } from './anexos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AnexoListComponent } from './components/anexo-list/anexo-list.component';
import { AnexoCreateComponent } from './components/anexo-create/anexo-create.component';
import { UpdateAnexoDialogComponent } from './components/anexo-list/dialogs/update-anexo-dialog/update-anexo-dialog.component';


@NgModule({
  declarations: [
    AnexoListComponent,
    AnexoCreateComponent,
    UpdateAnexoDialogComponent
  ],
  imports: [
    CommonModule,
    AnexosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class AnexosModule { }
