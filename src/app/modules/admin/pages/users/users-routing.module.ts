import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleAdminGuard } from 'src/app/guards/role-admin.guard';
import { GetUsersResolver } from './resolvers/get-users.resolver';
import { GetRolesResolver } from './resolvers/get-roles.resolver';
import { GetUserByidResolver } from './resolvers/get-user-byid.resolver';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [RoleAdminGuard],
    children: [
      {
        path: '',
        component: UserListComponent,
        resolve: {
          rslv: GetUsersResolver,
          roles: GetRolesResolver
        }
      },
      {
        path: 'create',
        component: UserCreateComponent,
        resolve: { rslv: GetRolesResolver }
      },
      {
        path: 'update/:idUser',
        component: UserUpdateComponent,
        resolve: { rslv: GetUserByidResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
