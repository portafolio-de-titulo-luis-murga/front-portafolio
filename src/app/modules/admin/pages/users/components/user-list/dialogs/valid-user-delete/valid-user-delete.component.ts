import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-valid-user-delete',
  templateUrl: './valid-user-delete.component.html',
  styleUrls: ['./valid-user-delete.component.css']
})
export class ValidUserDeleteComponent implements OnInit {

  public userSelected: FormControl;

  constructor(
    public dialogRef: MatDialogRef<ValidUserDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.userSelected = new FormControl(this.data.users[0]?.idUser, Validators.required);
  }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close({ resp: false });
  }

  onConfirm(): void {
    if (this.userSelected.valid) {
      this.dialogRef.close({ resp: true, userId: this.userSelected.value });
    }
  }

}
