import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidUserDeleteComponent } from './valid-user-delete.component';

describe('ValidUserDeleteComponent', () => {
  let component: ValidUserDeleteComponent;
  let fixture: ComponentFixture<ValidUserDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidUserDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidUserDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
