import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { filter, finalize, map, switchMap, take, tap } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UserService } from 'src/app/services/api/user.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';
import { JwtService } from 'src/app/services/common/jwt.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ValidUserDeleteComponent } from './dialogs/valid-user-delete/valid-user-delete.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public users: any[] = [];
  public allUsers: any[] = [];
  public roles: any[] = [];
  public idUserSession: number = 0;
  public roleSelectedFilter: number = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private swaService: SweetAlertService,
    private jwtService: JwtService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.users = this.activatedRoute.snapshot.data['rslv'];
    this.allUsers = this.activatedRoute.snapshot.data['rslv'];
    this.roles = this.activatedRoute.snapshot.data['roles'];
    this.getIdUserSession();
  }

  getIdUserSession() {
    const token = sessionStorage.getItem('tk');
    const payload = this.jwtService.getPayload(token);
    this.idUserSession = Number(payload.Id);
  }

  changeSelect(idRole: number): void {
    if (idRole !== 0) {
      this.users = this.allUsers.filter(user => user.idRole === idRole);
    } else {
      this.users = [...this.allUsers];
    }
  }

  goCreateUser(): void {
    this.router.navigate(['user', 'create']);
  }

  goUpdate(idUser: number): void {
    this.router.navigate(['user', 'update', idUser]);
  }

  resetPdw(idUser: number): void {
    this.spinner.show();

    this.userService.resetPwd(idUser).pipe(
      take(1),
      finalize(() => this.spinner.hide())
    ).subscribe(() => {
      this.swaService.sweetModal({
        title: 'Contraseña Reestablecida',
        text: '',
        buttonStyle: 'btn btn-success',
        confirmButtonText: 'Aceptar',
        icon: 'success'
      });
    });
  }

  userDelete(hasRelationUnits: number, idUser: number): void {
    if (hasRelationUnits) {
      const dialogResp = this.dialog.open(ValidUserDeleteComponent, {
        maxWidth: '1000px',
        minWidth: '400px',
        data: { users: this.users.filter(user => user.idUser !== idUser && user.roleName !== 'ADMIN') }
      });

      dialogResp.afterClosed().pipe(
        filter(data => !!data?.resp),
        tap(() => this.spinner.show()),
        take(1),
        map(data => data.userId),
        switchMap(userId => {
          return this.userService.deleteUserWithUnit(idUser, userId);
        }),
        finalize(() => this.spinner.hide())
      ).subscribe(users => {
        this.users = users;
        this.allUsers = users;
        this.roleSelectedFilter = 0;
      });
      return;
    }

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Debe confirmar',
        message: '¿Estás seguro de eliminar el usuario?'
      }
    });

    confirmDialog.afterClosed().pipe(
      filter(event => event === true),
      tap(() => this.spinner.show()),
      take(1),
      switchMap(() => {
        return this.userService.deleteUser(idUser);
      }),
      finalize(() => this.spinner.hide())
    ).subscribe(users => {
      this.users = users;
      this.allUsers = users;
      this.roleSelectedFilter = 0;
    });
  }

}
