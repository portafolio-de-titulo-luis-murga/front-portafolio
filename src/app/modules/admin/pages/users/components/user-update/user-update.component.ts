import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { finalize, take } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UserService } from 'src/app/services/api/user.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  public formUser: FormGroup;
  private idUser: number;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private swal: SweetAlertService
  ) {
    this.idUser = this.activatedRoute.snapshot.data['rslv']['idUser'];
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    const dataResolver = this.activatedRoute.snapshot.data['rslv'];
    this.formUser = this.fb.group({
      firstname: [dataResolver.firstName, [Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.maxLength(50), Validators.minLength(2)]],
      lastname: [dataResolver.lastName, [Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.maxLength(50), Validators.minLength(2)]],
      email: [dataResolver.email, [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[A-Za-z]{2,3}'), Validators.maxLength(250), Validators.minLength(5)]]
    });
  }

  updateUser(): void {
    if (this.formUser.valid) {
      this.spinner.show();

      const body = {
        firstName: this.formUser.value['firstname'],
        lastName: this.formUser.value['lastname'],
        email: this.formUser.value['email']
      };

      this.userService.updateUser(this.idUser, body).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {
        this.swal.sweetModal({
          title: 'Usuario Actualizado',
          text: '',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        });
      });
    }
  }

}
