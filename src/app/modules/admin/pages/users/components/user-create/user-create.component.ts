import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { filter, finalize, take } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UserService } from 'src/app/services/api/user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  public formUser: FormGroup;
  public rolList: any[] = [];
  public idRoleSelected: any;

  public hidePdw: boolean = true;
  public hideConfirmPdw: boolean = true;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.rolList = this.activatedRoute.snapshot.data['rslv'];
  }

  checkPasswords(group: FormGroup): any {
    let pass = group.get('pwd').value;
    let confirmPass = group.get('confirmPwd').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  initForm(): void {
    this.formUser = this.fb.group({
      username: ['', [Validators.required, Validators.pattern('^[a-z0-9]+$'), Validators.maxLength(15), Validators.minLength(4)]],
      firstname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.maxLength(50), Validators.minLength(2)]],
      lastname: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.maxLength(50), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[A-Za-z]{2,3}'), Validators.maxLength(250), Validators.minLength(5)]],
      rol: ['', [Validators.required]],
      // pwd: ['', [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[./$*])(?=(.*\\d){5})[A-Za-z\\d./$*]{8,}$'), Validators.maxLength(10), Validators.minLength(8)]],
      // confirmPwd: ['', [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[./$*])(?=(.*\\d){5})[A-Za-z\\d./$*]{8,}$'), Validators.maxLength(10), Validators.minLength(8)]]
    }
    // , { validators: this.checkPasswords }
    );
  }

  createUser(): void {
    if (this.formUser.valid) {
      const body = {
        username: this.formUser.value['username'].toLowerCase(),
        // pwd: this.formUser.value['pwd'],
        email: this.formUser.value['email'],
        firstName: this.formUser.value['firstname'].trim(),
        lastName: this.formUser.value['lastname'].trim(),
        idRole: this.formUser.value['rol']
      }
      this.spinner.show();
      this.userService.createUser(body).pipe(
        take(1),
        filter(isOk => isOk),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {
        this.formUser.reset();
      });

    }
  }

}
