import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { RoleService } from 'src/app/services/api/role.service';

@Injectable({
  providedIn: 'root'
})
export class GetRolesResolver implements Resolve<boolean> {

  constructor(
    private roleService: RoleService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.roleService.getRoles().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
