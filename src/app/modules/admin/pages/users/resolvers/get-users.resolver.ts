import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UserService } from 'src/app/services/api/user.service';

@Injectable({
  providedIn: 'root'
})
export class GetUsersResolver implements Resolve<boolean> {

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.userService.getUsers().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
