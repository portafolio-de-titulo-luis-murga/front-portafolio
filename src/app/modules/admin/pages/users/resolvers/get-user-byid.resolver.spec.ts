import { TestBed } from '@angular/core/testing';

import { GetUserByidResolver } from './get-user-byid.resolver';

describe('GetUserByidResolver', () => {
  let resolver: GetUserByidResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetUserByidResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
