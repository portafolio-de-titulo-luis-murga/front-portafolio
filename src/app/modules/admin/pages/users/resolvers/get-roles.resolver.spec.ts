import { TestBed } from '@angular/core/testing';

import { GetRolesResolver } from './get-roles.resolver';

describe('GetRolesResolver', () => {
  let resolver: GetRolesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetRolesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
