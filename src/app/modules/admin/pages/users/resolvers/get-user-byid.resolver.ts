import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UserService } from 'src/app/services/api/user.service';

@Injectable({
  providedIn: 'root'
})
export class GetUserByidResolver implements Resolve<boolean> {

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    const idUser = route.paramMap.get('idUser');
    return this.userService.getUserById(Number(idUser)).pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
