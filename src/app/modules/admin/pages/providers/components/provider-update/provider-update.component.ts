import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { filter, finalize, take } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProviderService } from 'src/app/services/api/provider.service';
import { NumberUtil } from 'src/app/shared/utils/number-util';

@Component({
  selector: 'app-provider-update',
  templateUrl: './provider-update.component.html',
  styleUrls: ['./provider-update.component.css']
})
export class ProviderUpdateComponent implements OnInit {

  public formProvider: FormGroup;
  public servicesArray = [];
  private idProvider: number;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private numberUtil: NumberUtil,
    private providerService: ProviderService,
    private spinner: NgxSpinnerService
  ) {
    this.idProvider = this.activatedRoute.snapshot.data['rslv']['idProvider'];
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    const dataResolver = this.activatedRoute.snapshot.data['rslv'];
    this.formProvider = this.fb.group({
      providerName: [dataResolver.nameProvider, [Validators.required, Validators.pattern('^[a-zA-Z0-9. ]+$'), Validators.maxLength(150), Validators.minLength(2)]],
      fixedCost: [dataResolver.fixedCost, [Validators.required, Validators.minLength(1)]],
      services: this.fb.array([])
    });

    const inputControlArray = this.formProvider.get('services') as FormArray;

    this.servicesArray = dataResolver.services;
    this.servicesArray.forEach(service => {
      inputControlArray.push(this.fb.control(service.costService, Validators.compose([Validators.required, Validators.minLength(1)])));
    });
  }

  updateProvider(): void {
    if (this.formProvider.valid) {
      const nameProvider = this.formProvider.value['providerName'].trim();
      const fixedCost = this.formProvider.value['fixedCost'];

      let servicesProviderCost: any[] = this.formProvider.value['services'];

      const dataServiceBody = servicesProviderCost.reduce((acc, cost, index) => {
        acc[`idService${index + 1}`] = this.servicesArray[index].idService;
        acc[`cost_service${index + 1}`] = this.numberUtil.thousandsToNumber(cost);
        return acc;
      }, {});

      const body = {
        idProvider: this.idProvider,
        nameProvider,
        fixedCost: this.numberUtil.thousandsToNumber(fixedCost),
        ...dataServiceBody
      };

      this.spinner.show();
      this.providerService.updateProvider(body).pipe(
        take(1),
        filter(isOk => isOk),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {});

    }
  }

}
