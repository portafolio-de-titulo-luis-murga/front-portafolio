import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailProviderServicesDialogComponent } from './detail-provider-services-dialog.component';

describe('DetailProviderServicesDialogComponent', () => {
  let component: DetailProviderServicesDialogComponent;
  let fixture: ComponentFixture<DetailProviderServicesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailProviderServicesDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailProviderServicesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
