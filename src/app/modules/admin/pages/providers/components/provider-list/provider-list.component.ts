import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { filter, finalize, map, switchMap, take, tap } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { DetailProviderServicesDialogComponent } from './dialogs/detail-provider-services-dialog/detail-provider-services-dialog.component';
import { ValidProviderDeleteComponent } from './dialogs/valid-provider-delete/valid-provider-delete.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ProviderService } from 'src/app/services/api/provider.service';

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.css']
})
export class ProviderListComponent implements OnInit {

  public providers: any[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private providerService: ProviderService
  ) { }

  ngOnInit(): void {
    this.providers = this.activatedRoute.snapshot.data['rslv'];
  }

  goCreateProvider(): void {
    this.router.navigate(['provider', 'create']);
  }

  showServices(providerName: string, services: any[]): MatDialogRef<DetailProviderServicesDialogComponent, any> {
    return this.dialog.open(DetailProviderServicesDialogComponent, {
      maxWidth: '1000px',
      minWidth: '400px',
      data: { providerName, services }
    });
  }

  providerDelete(hasRelationFaculties: number, idProvider: number): void {
    if (hasRelationFaculties) {
      const dialogResp = this.dialog.open(ValidProviderDeleteComponent, {
        maxWidth: '1000px',
        minWidth: '400px',
        data: { providers: this.providers.filter(provider => provider.idProvider !== idProvider) }
      });

      dialogResp.afterClosed().pipe(
        filter(data => !!data?.resp),
        tap(() => this.spinner.show()),
        take(1),
        map(data => data.providerId),
        switchMap(providerId => {
          return this.providerService.deleteProviderWithFaculty(idProvider, providerId);
        }),
        finalize(() => this.spinner.hide())
      ).subscribe(providers => {
        this.providers = providers;
      });
      return;
    }

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Debe confirmar',
        message: '¿Estás seguro de eliminar el proveedor?'
      }
    });

    confirmDialog.afterClosed().pipe(
      filter(event => event === true),
      tap(() => this.spinner.show()),
      take(1),
      switchMap(() => {
        return this.providerService.deleteProvider(idProvider);
      }),
      finalize(() => this.spinner.hide())
    ).subscribe(providers => {
      this.providers = providers;
    });

  }

  goUpdate(idProvider: number): void {
    this.router.navigate(['provider', 'update', idProvider]);
  }

}
