import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-detail-provider-services-dialog',
  templateUrl: './detail-provider-services-dialog.component.html',
  styleUrls: ['./detail-provider-services-dialog.component.css']
})
export class DetailProviderServicesDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailProviderServicesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

}
