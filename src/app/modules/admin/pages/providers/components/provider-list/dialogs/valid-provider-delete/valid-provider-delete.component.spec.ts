import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidProviderDeleteComponent } from './valid-provider-delete.component';

describe('ValidProviderDeleteComponent', () => {
  let component: ValidProviderDeleteComponent;
  let fixture: ComponentFixture<ValidProviderDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidProviderDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidProviderDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
