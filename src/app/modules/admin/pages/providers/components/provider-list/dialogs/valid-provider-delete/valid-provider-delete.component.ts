import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-valid-provider-delete',
  templateUrl: './valid-provider-delete.component.html',
  styleUrls: ['./valid-provider-delete.component.css']
})
export class ValidProviderDeleteComponent implements OnInit {

  public providerSelected: FormControl;

  constructor(
    public dialogRef: MatDialogRef<ValidProviderDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.providerSelected = new FormControl(this.data.providers[0]?.idProvider, Validators.required);
  }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close({ resp: false });
  }

  onConfirm(): void {
    if (this.providerSelected.valid) {
      this.dialogRef.close({ resp: true, providerId: this.providerSelected.value });
    }
  }

}
