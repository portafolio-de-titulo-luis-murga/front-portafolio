import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { filter, finalize, take } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProviderService } from 'src/app/services/api/provider.service';
import { NumberUtil } from 'src/app/shared/utils/number-util';

@Component({
  selector: 'app-provider-form',
  templateUrl: './provider-form.component.html',
  styleUrls: ['./provider-form.component.css']
})
export class ProviderFormComponent implements OnInit {

  public formProvider: FormGroup;
  public servicesArray = [];

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private numberUtil: NumberUtil,
    private providerService: ProviderService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.formProvider = this.fb.group({
      providerName: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9. ]+$'), Validators.maxLength(150), Validators.minLength(2)]],
      fixedCost: ['', [Validators.required, Validators.minLength(1)]],
      services: this.fb.array([])
    });

    const inputControlArray = this.formProvider.get('services') as FormArray;

    this.servicesArray = this.activatedRoute.snapshot.data['rslv'];
    this.servicesArray.forEach(() => {
      inputControlArray.push(this.fb.control('', Validators.compose([Validators.required, Validators.minLength(1)])));
    });
  }

  createProvider(): void {
    if (this.formProvider.valid) {
      const nameProvider = this.formProvider.value['providerName'].trim();
      const fixedCost = this.formProvider.value['fixedCost'];

      let servicesProviderCost: any[] = this.formProvider.value['services'];

      servicesProviderCost = servicesProviderCost.map((cost, index) => ({
        idService: this.servicesArray[index].idService,
        costService: this.numberUtil.thousandsToNumber(cost)
      }));

      const body = {
        nameProvider,
        fixedCost: this.numberUtil.thousandsToNumber(fixedCost),
        services: servicesProviderCost
      };

      this.spinner.show();
      this.providerService.createProvider(body).pipe(
        take(1),
        filter(isOk => isOk),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {
        this.formProvider.reset();
      });

    }
  }

}
