import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProvidersRoutingModule } from './providers-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProviderFormComponent } from './components/provider-form/provider-form.component';
import { ProviderListComponent } from './components/provider-list/provider-list.component';
import { DetailProviderServicesDialogComponent } from './components/provider-list/dialogs/detail-provider-services-dialog/detail-provider-services-dialog.component';
import { ValidProviderDeleteComponent } from './components/provider-list/dialogs/valid-provider-delete/valid-provider-delete.component';
import { ProviderUpdateComponent } from './components/provider-update/provider-update.component';


@NgModule({
  declarations: [
    ProviderFormComponent,
    ProviderListComponent,
    DetailProviderServicesDialogComponent,
    ValidProviderDeleteComponent,
    ProviderUpdateComponent
  ],
  imports: [
    CommonModule,
    ProvidersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProvidersModule { }
