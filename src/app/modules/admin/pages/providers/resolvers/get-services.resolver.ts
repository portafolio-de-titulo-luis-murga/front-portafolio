import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ServiceProviderService } from 'src/app/services/api/service-provider.service';

@Injectable({
  providedIn: 'root'
})
export class GetServicesResolver implements Resolve<boolean> {

  constructor(
    private serviceProviderService: ServiceProviderService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.serviceProviderService.getServices().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
