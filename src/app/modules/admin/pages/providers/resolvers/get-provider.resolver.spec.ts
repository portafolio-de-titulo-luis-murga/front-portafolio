import { TestBed } from '@angular/core/testing';

import { GetProviderResolver } from './get-provider.resolver';

describe('GetProviderResolver', () => {
  let resolver: GetProviderResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetProviderResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
