import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProviderService } from 'src/app/services/api/provider.service';

@Injectable({
  providedIn: 'root'
})
export class GetProviderResolver implements Resolve<boolean> {

  constructor(
    private providerService: ProviderService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    const idProvider = route.paramMap.get('idProvider');
    return this.providerService.getProviderById(Number(idProvider)).pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
