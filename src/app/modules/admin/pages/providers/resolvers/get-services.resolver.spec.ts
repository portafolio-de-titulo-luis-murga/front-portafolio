import { TestBed } from '@angular/core/testing';

import { GetServicesResolver } from './get-services.resolver';

describe('GetServicesResolver', () => {
  let resolver: GetServicesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetServicesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
