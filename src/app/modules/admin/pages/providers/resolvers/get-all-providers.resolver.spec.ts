import { TestBed } from '@angular/core/testing';

import { GetAllProvidersResolver } from './get-all-providers.resolver';

describe('GetAllProvidersResolver', () => {
  let resolver: GetAllProvidersResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetAllProvidersResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
