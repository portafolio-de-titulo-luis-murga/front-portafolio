import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProviderService } from 'src/app/services/api/provider.service';


@Injectable({
  providedIn: 'root'
})
export class GetAllProvidersResolver implements Resolve<boolean> {

  constructor(
    private providerService: ProviderService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.providerService.getProviders().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
