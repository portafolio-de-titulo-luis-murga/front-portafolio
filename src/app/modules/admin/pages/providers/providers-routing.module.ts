import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProviderListComponent } from './components/provider-list/provider-list.component';
import { ProviderFormComponent } from './components/provider-form/provider-form.component';
import { ProviderUpdateComponent } from './components/provider-update/provider-update.component';
import { GetAllProvidersResolver } from './resolvers/get-all-providers.resolver';
import { GetServicesResolver } from './resolvers/get-services.resolver';
import { GetProviderResolver } from './resolvers/get-provider.resolver';
import { RoleAdminGuard } from 'src/app/guards/role-admin.guard';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [RoleAdminGuard],
    children: [
      {
        path: '',
        component: ProviderListComponent,
        resolve: { rslv: GetAllProvidersResolver }
      },
      {
        path: 'create',
        component: ProviderFormComponent,
        resolve: { rslv: GetServicesResolver }
      },
      {
        path: 'update/:idProvider',
        component: ProviderUpdateComponent,
        resolve: { rslv: GetProviderResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvidersRoutingModule { }
