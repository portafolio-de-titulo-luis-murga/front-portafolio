import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable} from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UnitService } from 'src/app/services/api/unit.service';

@Injectable({
  providedIn: 'root'
})
export class GetUnitByIdResolver implements Resolve<boolean> {

  constructor(
    private unitService: UnitService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    const idUnit = route.paramMap.get('idUnit');
    return this.unitService.getUnitById(Number(idUnit)).pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
