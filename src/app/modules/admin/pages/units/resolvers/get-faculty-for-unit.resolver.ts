import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { FacultyService } from 'src/app/services/api/faculty.service';

@Injectable({
  providedIn: 'root'
})
export class GetFacultyForUnitResolver implements Resolve<boolean> {

  constructor(
    private facultyService: FacultyService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.facultyService.getFaculties().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
