import { TestBed } from '@angular/core/testing';

import { GetUnitByIdResolver } from './get-unit-by-id.resolver';

describe('GetUnitByIdResolver', () => {
  let resolver: GetUnitByIdResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetUnitByIdResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
