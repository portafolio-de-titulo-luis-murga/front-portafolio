import { TestBed } from '@angular/core/testing';

import { GetFacultyForUnitResolver } from './get-faculty-for-unit.resolver';

describe('GetFacultyForUnitResolver', () => {
  let resolver: GetFacultyForUnitResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetFacultyForUnitResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
