import { TestBed } from '@angular/core/testing';

import { GetSupervisorForUnitResolver } from './get-supervisor-for-unit.resolver';

describe('GetSupervisorForUnitResolver', () => {
  let resolver: GetSupervisorForUnitResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetSupervisorForUnitResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
