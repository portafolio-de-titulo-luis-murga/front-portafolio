import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UnitsRoutingModule } from './units-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UnitListComponent } from './components/unit-list/unit-list.component';
import { UnitCreateComponent } from './components/unit-create/unit-create.component';
import { DetailAnexosDialogComponent } from './components/unit-list/dialogs/detail-anexos-dialog/detail-anexos-dialog.component';
import { UnitUpdateComponent } from './components/unit-update/unit-update.component';


@NgModule({
  declarations: [
    UnitListComponent,
    UnitCreateComponent,
    DetailAnexosDialogComponent,
    UnitUpdateComponent
  ],
  imports: [
    CommonModule,
    UnitsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class UnitsModule { }
