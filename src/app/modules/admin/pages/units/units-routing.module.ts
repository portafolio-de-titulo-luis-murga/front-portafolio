import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleAdminGuard } from 'src/app/guards/role-admin.guard';
import { GetUnitsResolver } from '../../../../shared/resolver/get-units.resolver';
import { GetSupervisorForUnitResolver } from './resolvers/get-supervisor-for-unit.resolver';
import { GetFacultyForUnitResolver } from './resolvers/get-faculty-for-unit.resolver';
import { GetUnitByIdResolver } from './resolvers/get-unit-by-id.resolver';
import { UnitListComponent } from './components/unit-list/unit-list.component';
import { UnitCreateComponent } from './components/unit-create/unit-create.component';
import { UnitUpdateComponent } from './components/unit-update/unit-update.component';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [RoleAdminGuard],
    children: [
      {
        path: '',
        component: UnitListComponent,
        resolve: { rslv: GetUnitsResolver }
      },
      {
        path: 'create',
        component: UnitCreateComponent,
        resolve: {
          supervisor: GetSupervisorForUnitResolver,
          faculty: GetFacultyForUnitResolver
        }
      },
      {
        path: 'update/:idUnit',
        component: UnitUpdateComponent,
        resolve: {
          rslv: GetUnitByIdResolver,
          supervisor: GetSupervisorForUnitResolver,
          faculty: GetFacultyForUnitResolver
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitsRoutingModule { }
