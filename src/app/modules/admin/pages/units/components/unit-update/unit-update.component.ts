import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { finalize, take } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UnitService } from 'src/app/services/api/unit.service';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';

@Component({
  selector: 'app-unit-update',
  templateUrl: './unit-update.component.html',
  styleUrls: ['./unit-update.component.css']
})
export class UnitUpdateComponent implements OnInit {

  public formUnit: FormGroup;
  private idUnit: number;
  public faculties: any[] = [];
  public supervisors: any[] = [];

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private unitService: UnitService,
    private spinner: NgxSpinnerService,
    private swal: SweetAlertService
  ) {
    this.idUnit = this.activatedRoute.snapshot.data['rslv']['idUnit'];
    this.faculties = this.activatedRoute.snapshot.data['faculty'];
    this.supervisors = this.activatedRoute.snapshot.data['supervisor'];
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    const dataResolver = this.activatedRoute.snapshot.data['rslv'];
    this.formUnit = this.fb.group({
      name: [dataResolver.unitName, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(200), Validators.minLength(5)]],
      faculty: [dataResolver.idFaculty, [Validators.required]],
      supervisor: [dataResolver.idUser, [Validators.required]]
    });
  }

  updateUnit(): void {
    if (this.formUnit.valid) {
      this.spinner.show();

      const body = {
        unitName: this.formUnit.value['name'],
        idFaculty: Number(this.formUnit.value['faculty']),
        idUser: Number(this.formUnit.value['supervisor'])
      };

      this.unitService.updateUnit(this.idUnit, body).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {});
    }
  }

}
