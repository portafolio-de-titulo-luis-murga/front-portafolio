import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { filter, finalize, take } from 'rxjs/operators';

import { UnitService } from 'src/app/services/api/unit.service';

@Component({
  selector: 'app-unit-create',
  templateUrl: './unit-create.component.html',
  styleUrls: ['./unit-create.component.css']
})
export class UnitCreateComponent implements OnInit {

  public faculties: any[] = [];
  public supervisors: any[] = [];

  public formUnit: FormGroup;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private unitService: UnitService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.faculties = this.activatedRoute.snapshot.data['faculty'];
    this.supervisors = this.activatedRoute.snapshot.data['supervisor'];
  }

  initForm(): void {
    this.formUnit = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(200), Validators.minLength(5)]],
      faculty: ['', [Validators.required]],
      supervisor: ['', [Validators.required]]
      // pwd: ['', [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[./$*])(?=(.*\\d){5})[A-Za-z\\d./$*]{8,}$'), Validators.maxLength(10), Validators.minLength(8)]],
      // confirmPwd: ['', [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[./$*])(?=(.*\\d){5})[A-Za-z\\d./$*]{8,}$'), Validators.maxLength(10), Validators.minLength(8)]]
    }
    // , { validators: this.checkPasswords }
    );
  }

  createUnit(): void {
    if (this.formUnit.valid) {
      this.spinner.show();

      const body = {
        unitName: this.formUnit.value['name'],
        idFaculty: Number(this.formUnit.value['faculty']),
        idUser: Number(this.formUnit.value['supervisor'])
      };

      this.unitService.createUnit(body).pipe(
        take(1),
        filter(isOk => isOk),
        finalize(() => this.spinner.hide())
      ).subscribe(() => {
        this.formUnit.reset();
      });
    }
  }

}
