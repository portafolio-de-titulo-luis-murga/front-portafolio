import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAnexosDialogComponent } from './detail-anexos-dialog.component';

describe('DetailAnexosDialogComponent', () => {
  let component: DetailAnexosDialogComponent;
  let fixture: ComponentFixture<DetailAnexosDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailAnexosDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAnexosDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
