import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, finalize, switchMap, take, tap } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { DetailAnexosDialogComponent } from './dialogs/detail-anexos-dialog/detail-anexos-dialog.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { UnitService } from 'src/app/services/api/unit.service';

@Component({
  selector: 'app-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.css']
})
export class UnitListComponent implements OnInit {

  public units: any[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private unitService: UnitService
  ) { }

  ngOnInit(): void {
    this.units = this.activatedRoute.snapshot.data['rslv'];
  }

  goCreateUnit(): void {
    this.router.navigate(['unit', 'create']);
  }

  showAnexos(unitName: string, anexos: any[]): MatDialogRef<DetailAnexosDialogComponent, any> {
    return this.dialog.open(DetailAnexosDialogComponent, {
      maxWidth: '1000px',
      minWidth: '400px',
      data: { unitName, anexos }
    });
  }

  unitDelete(idUnit: number): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Debe confirmar',
        message: '¿Estás seguro de eliminar la unidad?'
      }
    });

    confirmDialog.afterClosed().pipe(
      filter(event => event === true),
      tap(() => this.spinner.show()),
      take(1),
      switchMap(() => {
        return this.unitService.deleteUnit(idUnit);
      }),
      finalize(() => this.spinner.hide())
    ).subscribe(units => {
      this.units = units;
    });
  }

  goUpdate(idUnit: number): void {
    this.router.navigate(['unit', 'update', idUnit]);
  }

}
