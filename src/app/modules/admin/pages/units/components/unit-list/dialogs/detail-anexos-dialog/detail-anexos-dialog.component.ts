import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-detail-anexos-dialog',
  templateUrl: './detail-anexos-dialog.component.html',
  styleUrls: ['./detail-anexos-dialog.component.css']
})
export class DetailAnexosDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailAnexosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

}
