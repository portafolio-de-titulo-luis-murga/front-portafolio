import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleSupervisorGuard } from 'src/app/guards/role-supervisor.guard';
import { ResourceCountResolver } from 'src/app/shared/resolver/resource-count.resolver';
import { GetAnexosSupervisorResolver } from './pages/resolvers/get-anexos-supervisor.resolver';
import { GetUnitsSupervisorResolver } from './pages/resolvers/get-units-supervisor.resolver';
import { ConsultPricingComponent } from './pages/consult-pricing/consult-pricing.component';
import { GraphicsComponent } from 'src/app/shared/components/graphics/graphics.component';
import { ChangePwdComponent } from 'src/app/shared/components/change-pwd/change-pwd.component';
import { UnitsSupervisorComponent } from './pages/units-supervisor/units-supervisor.component';

const routes: Routes = [
  {
    path: 'home',
    component: GraphicsComponent,
    resolve: { rslv: ResourceCountResolver }
  },
  {
    path: 'cambiarClave',
    component: ChangePwdComponent
  },
  {
    path: '',
    canActivateChild: [RoleSupervisorGuard],
    children: [
      {
        path: 'unit',
        component: UnitsSupervisorComponent,
        resolve: { rslv: GetUnitsSupervisorResolver }
      },
      {
        path: 'tarificacion',
        component: ConsultPricingComponent,
        resolve: { rslv: GetAnexosSupervisorResolver }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupervisorRoutingModule { }
