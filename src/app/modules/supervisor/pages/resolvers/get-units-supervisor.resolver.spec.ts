import { TestBed } from '@angular/core/testing';

import { GetUnitsSupervisorResolver } from './get-units-supervisor.resolver';

describe('GetUnitsSupervisorResolver', () => {
  let resolver: GetUnitsSupervisorResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetUnitsSupervisorResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
