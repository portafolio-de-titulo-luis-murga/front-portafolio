import { TestBed } from '@angular/core/testing';

import { GetAnexosSupervisorResolver } from './get-anexos-supervisor.resolver';

describe('GetAnexosSupervisorResolver', () => {
  let resolver: GetAnexosSupervisorResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetAnexosSupervisorResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
