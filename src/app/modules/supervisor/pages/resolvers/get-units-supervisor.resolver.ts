import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UnitService } from 'src/app/services/api/unit.service';
import { JwtService } from 'src/app/services/common/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class GetUnitsSupervisorResolver implements Resolve<boolean> {

  constructor(
    private unitService: UnitService,
    private spinner: NgxSpinnerService,
    private jwtService: JwtService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const token = sessionStorage.getItem('tk');
    const payload = this.jwtService.getPayload(token);

    this.spinner.show();
    return this.unitService.getUnits().pipe(
      map(data => {
        return data.filter(item => item.idUser === Number(payload.Id));
      }),
      finalize(() => this.spinner.hide())
    );
  }
}
