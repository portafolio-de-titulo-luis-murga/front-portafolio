import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { AnexoService } from 'src/app/services/api/anexo.service';
import { JwtService } from 'src/app/services/common/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class GetAnexosSupervisorResolver implements Resolve<boolean> {

  constructor(
    private anexoService: AnexoService,
    private spinner: NgxSpinnerService,
    private jwtService: JwtService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const token = sessionStorage.getItem('tk');
    const payload = this.jwtService.getPayload(token);
    const idUser = Number(payload.Id);

    this.spinner.show();
    return this.anexoService.getAnexosBySupervisor(idUser).pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
