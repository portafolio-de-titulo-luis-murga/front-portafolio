import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCallsDialogComponent } from './detail-calls-dialog.component';

describe('DetailCallsDialogComponent', () => {
  let component: DetailCallsDialogComponent;
  let fixture: ComponentFixture<DetailCallsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailCallsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCallsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
