import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-detail-calls-dialog',
  templateUrl: './detail-calls-dialog.component.html',
  styleUrls: ['./detail-calls-dialog.component.css']
})
export class DetailCallsDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailCallsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

}
