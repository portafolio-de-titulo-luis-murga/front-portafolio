import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-detail-provider-dialog',
  templateUrl: './detail-provider-dialog.component.html',
  styleUrls: ['./detail-provider-dialog.component.css']
})
export class DetailProviderDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailProviderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

}
