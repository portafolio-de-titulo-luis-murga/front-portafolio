import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTarificacionDialogComponent } from './detail-tarificacion-dialog.component';

describe('DetailTarificacionDialogComponent', () => {
  let component: DetailTarificacionDialogComponent;
  let fixture: ComponentFixture<DetailTarificacionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailTarificacionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailTarificacionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
