import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-detail-tarificacion-dialog',
  templateUrl: './detail-tarificacion-dialog.component.html',
  styleUrls: ['./detail-tarificacion-dialog.component.css']
})
export class DetailTarificacionDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailTarificacionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

}
