import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultPricingComponent } from './consult-pricing.component';

describe('ConsultPricingComponent', () => {
  let component: ConsultPricingComponent;
  let fixture: ComponentFixture<ConsultPricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultPricingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
