import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { DayJS } from 'src/app/shared/utils/date-util';
import { AnexoService } from 'src/app/services/api/anexo.service';
import { MatDialog } from '@angular/material/dialog';
import { DetailCallsDialogComponent } from './dialogs/detail-calls-dialog/detail-calls-dialog.component';
import { SweetAlertService } from 'src/app/services/common/sweet-alert.service';
import { DetailProviderDialogComponent } from './dialogs/detail-provider-dialog/detail-provider-dialog.component';
import { JwtService } from 'src/app/services/common/jwt.service';
import { DetailTarificacionDialogComponent } from './dialogs/detail-tarificacion-dialog/detail-tarificacion-dialog.component';

declare const $: any;

@Component({
  selector: 'app-consult-pricing',
  templateUrl: './consult-pricing.component.html',
  styleUrls: ['./consult-pricing.component.css']
})
export class ConsultPricingComponent implements OnInit, AfterViewInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  dataTable: any[] = [];

  rangeDate: FormGroup;

  minDate: Date;
  maxDate: Date;

  divHaveClass: boolean = true;
  isXsView: boolean = false;
  loadingDataTable: boolean = true;

  constructor(
    private fb: FormBuilder,
    private breakpointObserver: BreakpointObserver,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private anexoService: AnexoService,
    private dialog: MatDialog,
    private swaService: SweetAlertService,
    private jwtService: JwtService
  ) {
    this.dataTable = this.activatedRoute.snapshot.data['rslv'];
  }

  ngOnInit(): void {

    this.observerResolution();
    this.initFormDatepicker();
  }

  ngAfterViewInit(): void {
    this.loadTable();
  }

  loadTable(): void {
    this.loadingDataTable = false;
    setTimeout(() => {
      // Cargando tabla
      $('#consult').DataTable({
        pagingType: "full_numbers",
        ordering: false,
        oLanguage: {
          sLengthMenu: "Mostrar _MENU_ registros",
          sEmptyTable: "Sin datos disponibles",
          sInfoEmpty: "Sin resgistros para mostrar",
          sLoadingRecords: "Cargando...",
          sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
          sSearch: 'Buscador:',
          sInfoFiltered: "(filtrado de _MAX_ registros en total)",
          sZeroRecords: "No se encontraron registros coincidentes",
          oPaginate: {
            sFirst: "Primero",
            sLast: "Último",
            sNext: "Siguiente",
            sPrevious: "Anterior"
          }
        },
        columnDefs: [
          { searchable: false, targets: [3,4,5] },
        ],
        lengthMenu: [
          [5, 10, 25, 50, -1],
          [5, 10, 25, 50, "Todo"]
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar",
        }

      });

      $('.card .material-datatables label').addClass('form-group');
    });
  }

  initFormDatepicker(): void {
    const lastDate = DayJS();
    this.maxDate = lastDate.toDate();
    this.minDate = lastDate.subtract(5, 'year').set('month', 0).toDate();

    this.rangeDate = this.fb.group({
      start: [this.maxDate, Validators.required],
      end: [this.maxDate, Validators.required],
    });
  }

  observerResolution(): void {
    // Escucha cuando la resolución es diferente a la más baja
    const suscript = this.breakpointObserver.observe(
      [Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small]
    ).pipe(takeUntil(this.unsubscribe$)).subscribe(result => {
      this.divHaveClass = result.matches;
    });
    this.subscription.add(suscript);
    this.divHaveClass = this.breakpointObserver.isMatched([
      Breakpoints.XLarge, Breakpoints.Large, Breakpoints.Medium, Breakpoints.Small
    ]);

    // Escucha cuando la resolución es la más baja
    const suscript2 = this.breakpointObserver.observe(Breakpoints.XSmall).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(result => {
      this.isXsView = result.matches;
    });
    this.subscription.add(suscript2);
    this.isXsView = this.breakpointObserver.isMatched(Breakpoints.XSmall);
  }

  detailCalls(idAnexo: number): void {
    if (this.rangeDate.valid) {
      this.spinner.show();
      const initDate = DayJS(this.rangeDate.value['start']).format('YYYY-MM-DD');
      const lastDate = DayJS(this.rangeDate.value['end']).format('YYYY-MM-DD');

      this.anexoService.getDetailCalls(idAnexo, initDate, lastDate).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(resp => {
        if (resp.code === '001') {
          this.swaService.sweetModal({
            title: 'El Anexo no tiene llamadas para el periodo seleccionado',
            text: '',
            buttonStyle: 'btn btn-info',
            confirmButtonText: 'Aceptar',
            icon: 'info'
          });
        } else {
          this.dialog.open(DetailCallsDialogComponent, {
            maxWidth: '1000px',
            minWidth: '400px',
            data: {
              idAnexo,
              detail: resp.data
            }
          });
        }
      });
    } else {
      this.swaService.sweetModal({
        title: 'Debe seleccionar un rango de fechas válido',
        text: '',
        buttonStyle: 'btn btn-danger',
        confirmButtonText: 'Aceptar',
        icon: 'error'
      });
    }
  }

  detailProvider(nameProvider: string, serviceList: any[]): void {
    this.dialog.open(DetailProviderDialogComponent, {
      maxWidth: '1000px',
      minWidth: '400px',
      data: { nameProvider, serviceList }
    });
  }

  detailTarificacion(idAnexo: number): void {
    if (this.rangeDate.valid) {
      this.spinner.show();
      const initDate = DayJS(this.rangeDate.value['start']).format('YYYY-MM-DD');
      const lastDate = DayJS(this.rangeDate.value['end']).format('YYYY-MM-DD');

      const token = sessionStorage.getItem('tk');
      const payload = this.jwtService.getPayload(token);

      const body = {
        idUser: Number(payload.Id),
        idAnexo,
        initDate,
        lastDate
      };

      this.anexoService.getDetailTarificacion(body).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe(resp => {
        this.dialog.open(DetailTarificacionDialogComponent, {
          maxWidth: '1000px',
          minWidth: '400px',
          data: {
            idAnexo,
            detail: resp.services
          }
        });
      });
    } else {
      this.swaService.sweetModal({
        title: 'Debe seleccionar un rango de fechas válido',
        text: '',
        buttonStyle: 'btn btn-danger',
        confirmButtonText: 'Aceptar',
        icon: 'error'
      });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
