import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitsSupervisorComponent } from './units-supervisor.component';

describe('UnitsSupervisorComponent', () => {
  let component: UnitsSupervisorComponent;
  let fixture: ComponentFixture<UnitsSupervisorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnitsSupervisorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitsSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
