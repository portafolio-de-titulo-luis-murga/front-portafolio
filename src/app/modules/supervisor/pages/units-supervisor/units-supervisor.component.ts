import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

import { DetailAnexosDialogComponent } from 'src/app/modules/admin/pages/units/components/unit-list/dialogs/detail-anexos-dialog/detail-anexos-dialog.component';

@Component({
  selector: 'app-units-supervisor',
  templateUrl: './units-supervisor.component.html',
  styleUrls: ['./units-supervisor.component.css']
})
export class UnitsSupervisorComponent implements OnInit {

  public units: any[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.units = this.activatedRoute.snapshot.data['rslv'];
  }

  showAnexos(unitName: string, anexos: any[]): MatDialogRef<DetailAnexosDialogComponent, any> {
    return this.dialog.open(DetailAnexosDialogComponent, {
      maxWidth: '1000px',
      minWidth: '400px',
      data: { unitName, anexos }
    });
  }

}
