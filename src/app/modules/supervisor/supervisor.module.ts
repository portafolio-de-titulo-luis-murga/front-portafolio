import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SupervisorRoutingModule } from './supervisor-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConsultPricingComponent } from './pages/consult-pricing/consult-pricing.component';
import { DetailCallsDialogComponent } from './pages/consult-pricing/dialogs/detail-calls-dialog/detail-calls-dialog.component';
import { DetailProviderDialogComponent } from './pages/consult-pricing/dialogs/detail-provider-dialog/detail-provider-dialog.component';
import { DetailTarificacionDialogComponent } from './pages/consult-pricing/dialogs/detail-tarificacion-dialog/detail-tarificacion-dialog.component';
import { UnitsSupervisorComponent } from './pages/units-supervisor/units-supervisor.component';


@NgModule({
  declarations: [
    ConsultPricingComponent,
    DetailCallsDialogComponent,
    DetailProviderDialogComponent,
    DetailTarificacionDialogComponent,
    UnitsSupervisorComponent
  ],
  imports: [
    CommonModule,
    SupervisorRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SupervisorModule { }
