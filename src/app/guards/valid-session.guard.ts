import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';

import { JwtService } from '../services/common/jwt.service';
import { LoginService } from '../services/api/login.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidSessionGuard implements CanActivate, CanLoad {

  constructor(
    private jtwtService: JwtService,
    private loginService: LoginService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.isAuthenticated();
  }

  canLoad(route: Route): boolean {
    return this.isAuthenticated();
  }

  private isAuthenticated(): boolean {
    const token = sessionStorage.getItem('tk');
    const payload = this.jtwtService.getPayload(token);
    const timeRemaining = this.jtwtService.getTimeRemaining(token);

    if (!payload || !timeRemaining || timeRemaining <= 0) {
      this.loginService.logOut();
      return false;
    }
    this.jtwtService.checkExpTime();
    return true;
  }

}
