import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { JwtService } from '../services/common/jwt.service';
import { LoginService } from '../services/api/login.service';
import { RoleEnum } from '../types/enums/role.enum';

@Injectable({
  providedIn: 'root'
})
export class RoleAdminGuard implements CanActivateChild {

  constructor(private jtwtService: JwtService, private loginService: LoginService) {}

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const token = sessionStorage.getItem('tk');
    const { role } = this.jtwtService.getPayload(token);
    if (role !== RoleEnum.ADMIN) {
      this.loginService.logOut();
      return false;
    }
    return true;
  }

}
