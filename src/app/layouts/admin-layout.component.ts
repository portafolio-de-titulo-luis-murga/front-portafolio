import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import PerfectScrollbar from 'perfect-scrollbar';

import { NavbarComponent } from '../shared/components/navbar/navbar.component';

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html'
})
export class AdminLayoutComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(NavbarComponent, {static: false}) navbar: NavbarComponent;
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.runOnRouteChange();
    this.scrollForNavitationEnd();
  }

  ngAfterViewInit(): void {
    this.runOnRouteChange();
  }

  scrollForNavitationEnd(): void {
    const suscrip = this.router.events.pipe(
      takeUntil(this.unsubscribe$),
      filter(event => event instanceof NavigationEnd)
      ).subscribe(() => {
      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      window.scrollTo(0, 0);
      elemMainPanel.scrollTop = 0;
      elemSidebar.scrollTop = 0;
      this.navbar.sidebarClose();
    });
    this.subscription.add(suscrip);
  }

  runOnRouteChange(): void {
    const html = document.getElementsByTagName('html')[0];
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      let ps = new PerfectScrollbar(elemMainPanel);
      ps = new PerfectScrollbar(elemSidebar);
      ps.update();
      html.classList.add('perfect-scrollbar-on');
    } else {
      html.classList.add('perfect-scrollbar-off');
    }
  }

  /**
   * @description Función que ayuda a indicar si el equipo es mac o ipad
   */
  isMac(): boolean {
    return navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }
}
