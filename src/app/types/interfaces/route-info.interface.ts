export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  show?: boolean;
  children?: ChildrenItems[];
}
