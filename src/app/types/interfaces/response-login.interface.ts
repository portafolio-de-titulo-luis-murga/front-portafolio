export interface IDataResponseLogin {
  role: string;
  token: string;
  refreshToken: string;
}
