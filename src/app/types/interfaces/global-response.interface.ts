export interface IGlobalResponse<T> {
  code: string;
  message: string;
  data: T;
}
