import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient
  ) { }

  getRoles(): Observable<any> {
    return this.http.get<any>(`${this.host}/Role`).pipe(
      map(resp => resp.data)
    );
  }
}
