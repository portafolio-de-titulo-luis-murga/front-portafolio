import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { IDataResponseLogin } from '../../types/interfaces/response-login.interface';
import { IGlobalResponse } from 'src/app/types/interfaces/global-response.interface';
import { RoleEnum } from 'src/app/types/enums/role.enum';
import { ITEMS_ADMIN, ITEMS_SUPERVISOR } from 'src/app/shared/constants/sidebar-items';
import { JwtService } from '../common/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
    private router: Router,
    private jwtService: JwtService
  ) { }

  cleanStorage(): void {
    sessionStorage.removeItem('tk');
    sessionStorage.removeItem('rtk');
  }

  saveTokens(data: IDataResponseLogin): void {
    const { token, refreshToken } = data;
    sessionStorage.setItem('tk', token);
    sessionStorage.setItem('rtk', refreshToken);
  }

  menuRole(): any {
    const token = sessionStorage.getItem('tk');
    const { role } = this.jwtService.getPayload(token);
    const dictionaryRoute = {
      [RoleEnum.ADMIN]: ITEMS_ADMIN,
      [RoleEnum.SUPERVISOR]: ITEMS_SUPERVISOR
    };
    return dictionaryRoute[role];
  }

  routeRole(): void {
    const token = sessionStorage.getItem('tk');
    const { role } = this.jwtService.getPayload(token);
    if (role === 'ADMIN') {
      this.router.navigate(['home']);
    } else if (role === 'SUPERVISOR') {
      this.router.navigate(['responsable', 'home']);
    }
  }

  validLogin(response: IGlobalResponse<IDataResponseLogin>): void {
    const { code, data } = response;
    if (code === '000') {
      this.saveTokens(data);
      this.menuRole();
      this.routeRole();
    } else if (code !== '001') {
      throw new HttpErrorResponse({ error: response, status: 500 });
    }
  }

  login(credentials: any): Observable<boolean> {
    return this.http.post<IGlobalResponse<IDataResponseLogin>>(`${this.host}/Auth/login`, credentials).pipe(
      tap(data => this.validLogin(data)),
      map(data => data.code === '000')
    );
  }

  logOut(): void {
    this.cleanStorage();
    this.jwtService.stopCheckExp();
    this.router.navigate(['']);
  }

}
