import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { SweetAlertService } from '../common/sweet-alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
    private swaService: SweetAlertService,
    private router: Router
  ) { }

  getProviders(): Observable<any> {
    return this.http.get<any>(`${this.host}/Provider`).pipe(
      map(resp => resp.data)
    );
  }

  getProviderById(idProvider: number): Observable<any> {
    return this.http.get<any>(`${this.host}/Provider/${idProvider}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['provider']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validCreateUpdateProvider(resp: any, type: string): void {
    const { code } = resp;
    if (code === '000') {
      this.swaService.sweetModal({
        title: `Proveedor ${type === 'create' ? 'Creado' : 'Actualizado' }`,
        text: '',
        buttonStyle: 'btn btn-success',
        confirmButtonText: 'Aceptar',
        icon: 'success'
      });
    } else if (code === '001') {
      this.swaService.sweetModal({
        title: 'Nombre ya utilizado por otro proveedor',
        text: '',
        buttonStyle: 'btn btn-info',
        confirmButtonText: 'Aceptar',
        icon: 'info'
      });
    } else {
      throw new HttpErrorResponse({ error: resp, status: 500 });
    }
  }

  createProvider(body: any): Observable<any> {
    return this.http.post<any>(`${this.host}/Provider`, body).pipe(
      tap(resp => this.validCreateUpdateProvider(resp, 'create')),
      map(resp => resp.code === '000')
    );
  }

  deleteProvider(idProvider: number): Observable<any> {
    return this.http.delete<any>(`${this.host}/Provider/${idProvider}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  deleteProviderWithFaculty(idProvider: number, newIdProvider: number): Observable<any> {
    return this.http.delete<any>(`${this.host}/Provider/${idProvider}/${newIdProvider}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  updateProvider(body: any): Observable<any> {
    return this.http.put<any>(`${this.host}/Provider`, body).pipe(
      tap(resp => this.validCreateUpdateProvider(resp, 'update')),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['provider']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }
}
