import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient
  ) { }

  getFaculties(): Observable<any> {
    return this.http.get<any>(`${this.host}/Faculty`).pipe(
      map(resp => resp.data)
    );
  }

  getUniversityReport(dateFilter: string): Observable<any> {
    const headers = new HttpHeaders({
      monthYear: dateFilter
    });
    return this.http.get<any>(`${this.host}/Faculty/reportUniversity`, { headers }).pipe(
      map(resp => resp.data)
    );
  }

  resourceCount(): Observable<any> {
    return this.http.get<any>(`${this.host}/Faculty/count`).pipe(
      map(resp => resp.data)
    );
  }
}
