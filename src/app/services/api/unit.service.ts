import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { SweetAlertService } from '../common/sweet-alert.service';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
    private swaService: SweetAlertService,
    private router: Router
  ) { }

  getUnitsReport(initDate: string, lastDate: string): Observable<any> {
    return this.http.get<any>(`${this.host}/Unit/report/${initDate}/${lastDate}`).pipe(
      map(resp => resp.data)
    );
  }

  getUnits(): Observable<any> {
    return this.http.get<any>(`${this.host}/Unit`).pipe(
      map(resp => resp.data)
    );
  }

  getUnitById(idUnit: number): Observable<any> {
    return this.http.get<any>(`${this.host}/Unit/${idUnit}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['unit']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validCreateUnit(resp: any): void {
    const { code } = resp;
    let data;
    switch (code) {
      case '000':
        data = {
          title: 'Unidad Creada',
          text: '',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        };
        break;
      case '001':
        data = {
          title: 'Nombre de Unidad ya utilizado',
          text: 'Por favor cambie el nombre ingresado',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      default:
        throw new HttpErrorResponse({ error: resp, status: 500 });
    }

    this.swaService.sweetModal(data);
  }

  createUnit(body: any): Observable<any> {
    return this.http.post<any>(`${this.host}/Unit`, body).pipe(
      tap(resp => this.validCreateUnit(resp)),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['unit']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  deleteUnit(idUnit: number): Observable<any> {
    return this.http.delete<any>(`${this.host}/Unit/${idUnit}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validUpdateUnit(resp: any): void {
    const { code } = resp;
    let data;
    switch (code) {
      case '000':
        data = {
          title: 'Unidad Actualizada',
          text: '',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        };
        break;
      case '002':
        data = {
          title: 'Nombre de Unidad ya utilizado',
          text: 'Por favor cambie el nombre ingresado',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      default:
        throw new HttpErrorResponse({ error: resp, status: 500 });
    }

    this.swaService.sweetModal(data);
  }

  updateUnit(idUnit: number, body: any): Observable<any> {
    return this.http.put<any>(`${this.host}/Unit/${idUnit}`, body).pipe(
      tap(resp => this.validUpdateUnit(resp)),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['unit']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

}
