import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from "rxjs/operators";

import { environment } from 'src/environments/environment';
import { SweetAlertService } from '../common/sweet-alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
    private swaService: SweetAlertService,
    private router: Router
  ) { }

  getSupervisors(): Observable<any> {
    return this.http.get<any>(`${this.host}/User/responsable`).pipe(
      map(resp => resp.data)
    );
  }

  getUsers(): Observable<any> {
    return this.http.get<any>(`${this.host}/User`).pipe(
      map(resp => resp.data)
    );
  }

  getUserById(idUser: number): Observable<any> {
    return this.http.get<any>(`${this.host}/User/${idUser}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['user']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validCreateUser(resp: any): void {
    const { code } = resp;
    let data;
    switch (code) {
      case '000':
        data = {
          title: 'Usuario Creado',
          text: '',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        };
        break;
      case '001':
        data = {
          title: 'Username ya utilizado',
          text: '',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      case '002':
        data = {
          title: 'Rol no existe',
          text: '',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      default:
        throw new HttpErrorResponse({ error: resp, status: 500 });
    }

    this.swaService.sweetModal(data);
  }

  createUser(body: any): Observable<any> {
    return this.http.post<any>(`${this.host}/User`, body).pipe(
      tap(resp => this.validCreateUser(resp)),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['user']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  updateUser(idUser: number, body: any): Observable<any> {
    return this.http.put<any>(`${this.host}/User/${idUser}`, body).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['user']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  deleteUser(idUser: number): Observable<any> {
    return this.http.delete<any>(`${this.host}/User/${idUser}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  deleteUserWithUnit(idUser: number, newIdUser: number): Observable<any> {
    return this.http.delete<any>(`${this.host}/User/${idUser}/${newIdUser}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  resetPwd(idUser: number): Observable<any> {
    return this.http.get<any>(`${this.host}/User/resetPassword/${idUser}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validUpdatePwd(resp: any): void {
    const { code } = resp;
    let data;
    switch (code) {
      case '000':
        data = {
          title: 'Cambio realizado',
          text: 'Su contraseña se ha modificado',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        };
        break;
      case '002':
        data = {
          title: 'Error',
          text: 'Su contraseña actual no es correcta',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      default:
        throw new HttpErrorResponse({ error: resp, status: 500 });
    }

    this.swaService.sweetModal(data);
  }

  changePwd(body: any): Observable<any> {
    return this.http.put<any>(`${this.host}/User/updatePwd`, body).pipe(
      tap(resp => this.validUpdatePwd(resp)),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

}
