import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceProviderService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
  ) { }

  getServices(): Observable<any> {
    return this.http.get<any>(`${this.host}/Service`).pipe(
      map(resp => resp.data)
    );
  }
}
