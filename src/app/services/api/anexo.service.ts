import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { SweetAlertService } from '../common/sweet-alert.service';

@Injectable({
  providedIn: 'root'
})
export class AnexoService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient,
    private swaService: SweetAlertService,
    private router: Router
  ) { }

  getAnexosReport(initDate: string, lastDate: string): Observable<any> {
    return this.http.get<any>(`${this.host}/Anexo/report/${initDate}/${lastDate}`).pipe(
      map(resp => resp.data)
    );
  }

  getAnexosBySupervisor(idUser: number): Observable<any> {
    return this.http.get<any>(`${this.host}/Anexo/supervisor/${idUser}`).pipe(
      map(resp => resp.data)
    );
  }

  getDetailCalls(idAnexo: number, initDate: string, lastDate: string): Observable<any> {
    return this.http.get<any>(`${this.host}/Anexo/dst/${idAnexo}/${initDate}/${lastDate}`);
  }

  getDetailTarificacion(body: any): Observable<any> {
    return this.http.post<any>(`${this.host}/Anexo/costAnexo`, body).pipe(
      map(resp => resp.data)
    );
  }

  getAnexos(): Observable<any> {
    return this.http.get<any>(`${this.host}/Anexo`).pipe(
      map(resp => resp.data)
    );
  }

  changeStatus(idAnexo: number): Observable<any> {
    return this.http.get<any>(`${this.host}/Anexo/statusChange/${idAnexo}`).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  validCreateAnexo(resp: any): void {
    const { code } = resp;
    let data;
    switch (code) {
      case '000':
        data = {
          title: 'Anexo Creado',
          text: '',
          buttonStyle: 'btn btn-success',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        };
        break;
      case '001':
        data = {
          title: 'Anexo ya existe',
          text: 'Por favor cambie el valor ingresado',
          buttonStyle: 'btn btn-info',
          confirmButtonText: 'Aceptar',
          icon: 'info'
        };
        break;
      default:
        throw new HttpErrorResponse({ error: resp, status: 500 });
    }

    this.swaService.sweetModal(data);
  }

  createAnexo(body: any): Observable<any> {
    return this.http.post<any>(`${this.host}/Anexo`, body).pipe(
      tap(resp => this.validCreateAnexo(resp)),
      map(resp => resp.code === '000'),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        this.router.navigate(['anexo']);
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

  updateAnexo(body: any): Observable<any> {
    return this.http.put<any>(`${this.host}/Anexo`, body).pipe(
      tap(resp => {
        const { code } = resp;
        if (code !== '000') {
          throw new HttpErrorResponse({ error: resp, status: 500 });
        }
      }),
      map(resp => resp.data),
      catchError((error: HttpErrorResponse) => {
        this.swaService.generalErrorService();
        return throwError(() => new HttpErrorResponse(error));
      })
    );
  }

}
