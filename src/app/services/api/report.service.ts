import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private host: string = environment.hostApi;

  constructor(
    private http: HttpClient
  ) { }

  generatePdf(idTemplate: string, data: any): Observable<any> {
    return this.http.post<any>(`${this.host}/Report/generatePdf`, { idTemplate, data }).pipe(
      map(resp => resp.data)
    );
  }

  getSupervisorExcel(dateFilter: string, idUser: number): Observable<any> {
    return this.http.post<any>(`${this.host}/User/report/excel/${idUser}`, { dateFilter }).pipe(
      map(resp => resp.data)
    );
  }

  getSupervisorTarification(dateFilter: string, idUser: number): Observable<any> {
    return this.http.post<any>(`${this.host}/User/report/${idUser}`, { dateFilter });
  }
}
