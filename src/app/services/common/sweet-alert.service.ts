import { Injectable } from '@angular/core';

import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor() { }

  closeAllModals(): void {
    swal.close();
  }

  sweetModal(modalContent: any): void {
    swal.fire({
      title: modalContent.title,
      text: modalContent.text,
      buttonsStyling: false,
      customClass: {
        confirmButton: modalContent.buttonStyle
      },
      confirmButtonText: modalContent.confirmButtonText,
      icon: modalContent.icon
    });
  }

  generalErrorService(): void {
    swal.fire({
      title: 'Lo sentimos',
      text: 'Ha ocurrido un error inesperado',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger'
      },
      confirmButtonText: 'Aceptar',
      icon: 'error'
    });
  }

}
