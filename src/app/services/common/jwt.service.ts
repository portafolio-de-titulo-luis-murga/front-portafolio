import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription, interval } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SweetAlertService } from './sweet-alert.service';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription;

  constructor(
    private router: Router,
    private swaService: SweetAlertService
  ) { }

  getPayload(jwt: string): any {
    try {
      const base64Url = jwt.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      return JSON.parse(window.atob(base64));
    } catch (error) {
      this.router.navigate(['']);
      this.swaService.sweetModal({
        title: 'Lo sentimos',
        text: 'Ha ocurrido un error inesperado',
        buttonStyle: 'btn btn-danger',
        confirmButtonText: 'Aceptar',
        icon: 'error'
      });
    }
  }

  getTimeRemaining(jwt: string): number {
    const { exp } = this.getPayload(jwt);
    const currentTime = Math.floor(Date.now() / 1000);
    const timeRemaining = exp - currentTime;
    return timeRemaining;
  }

  checkExpTime(): void {
    this.subscription = interval(5000).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      const token = sessionStorage.getItem('tk');
      const timeRem = this.getTimeRemaining(token);
      // console.log("tiempo exp: ", timeRem);
      if (timeRem <= 30) {
        this.stopCheckExp();
      }
    });
  }

  stopCheckExp(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
