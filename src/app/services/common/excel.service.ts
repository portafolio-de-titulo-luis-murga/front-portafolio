import { Injectable } from '@angular/core';

import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  exportToExcel(data: any[], fileName: string, headers: string[]): void {
    const jsonData = data.map(obj => {
      const newObj = {};
      headers.forEach((header, index) => {
        newObj[header] = obj[Object.keys(obj)[index]];
      });
      return newObj;
    });

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData, { header: headers });
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };

    // Ajustar el ancho de las columnas
    const columns = headers.map((header, index) => ({ wch: header.length > 12 ? header.length : 12 }));
    worksheet['!cols'] = columns;

    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const excelBlob: Blob = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    saveAs(excelBlob, fileName + '.xlsx');
  }
}
