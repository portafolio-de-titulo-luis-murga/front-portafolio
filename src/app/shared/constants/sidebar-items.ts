import { RouteInfo } from "src/app/types/interfaces/route-info.interface";

export const ITEMS_ADMIN: RouteInfo[] = [
  {
    path: '/home',
    title: 'Home',
    type: 'link',
    icontype: 'home'
  },
  {
    path: '',
    title: 'Mantenedores',
    type: 'sub',
    icontype: 'apps',
    collapse: 'components',
    show: true,
    children: [
      { path: '/user', title: 'Usuarios', ab: 'U' },
      { path: '/provider', title: 'Proveedores', ab:'P' },
      { path: '/unit', title: 'Unidades', ab:'U' },
      { path: '/anexo', title: 'Anexos', ab:'A' },
      // { path: '/account', title: 'Cuenta Presupuestaria', ab:'CP' }
    ]
  },
  {
    path: '',
    title: 'Reportería',
    type: 'sub',
    icontype: 'description',
    collapse: 'reports',
    show: true,
    children: [
      { path: '/report/general', title: 'General', ab: 'G' },
      { path: '/report/anexo-unidad', title: 'Tarificación Anexo/Unidad', ab:'AU' },
      { path: '/report/responsable', title: 'Tarificación Responsable', ab:'TR' },
    ]
  }
];

export const ITEMS_SUPERVISOR: RouteInfo[] = [
  {
    path: '/responsable/home',
    title: 'Home',
    type: 'link',
    icontype: 'home'
  },
  {
    path: '/responsable/unit',
    title: 'Mis Unidades',
    type: 'link',
    icontype: 'holiday_village'
  },
  {
    path: '/responsable/tarificacion',
    title: 'Consulta Tarificación',
    type: 'link',
    icontype: 'call'
  }
];
