import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc';
import * as timezone from 'dayjs/plugin/timezone';
import * as isSameOrAfter from 'dayjs/plugin/isSameOrAfter';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(isSameOrAfter);

/**
 * @description Funcion que retorna fecha/hora
 * actual con zona horaria de Santiago
 */
function DayJS(date?: string): dayjs.Dayjs {
  const day = date ? dayjs(date) : dayjs();
  return day.tz("America/Santiago");
}

export {
  DayJS
};
