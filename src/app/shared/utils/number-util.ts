import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class NumberUtil {

  thousandsToNumber(value: string): number {
    let valueString = typeof value !== 'string' ? Number(value).toString() : value;
    const withoutDot = valueString.split('.').join('');
    return Number(withoutDot);
  }
}
