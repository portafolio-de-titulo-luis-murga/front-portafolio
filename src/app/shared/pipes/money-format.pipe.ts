import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyFormat'
})
export class MoneyFormatPipe implements PipeTransform {

  transform(value: unknown): unknown {
    return value.toString().split('.').join('').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

}
