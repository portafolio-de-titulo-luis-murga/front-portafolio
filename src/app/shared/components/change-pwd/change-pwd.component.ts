import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NgxSpinnerService } from 'ngx-spinner';
import { finalize, take } from 'rxjs';

import { UserService } from 'src/app/services/api/user.service';

@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.css']
})
export class ChangePwdComponent implements OnInit {

  formPwd: FormGroup;

  public hideOldPdw: boolean = true;
  public hideNewPdw: boolean = true;
  public hideConfirmNewPdw: boolean = true;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  checkPasswords(group: FormGroup): any {
    let pass = group.get('newPwd').value;
    let confirmPass = group.get('confirmNewPwd').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  initForm(): void {
    this.formPwd = this.fb.group({
      oldPwd: ['', [Validators.required, Validators.pattern('^[a-z0-9]+$'), Validators.maxLength(10), Validators.minLength(3)]],
      newPwd: ['', [Validators.required, Validators.pattern('^[a-z0-9]+$'), Validators.maxLength(10), Validators.minLength(3)]],
      confirmNewPwd: ['', [Validators.required, Validators.pattern('^[a-z0-9]+$'), Validators.maxLength(10), Validators.minLength(3)]]
    }, { validators: this.checkPasswords });
  }

  changePwd(): void {
    if (this.formPwd.valid) {
      this.spinner.show();

      const body = {
        oldPwd: this.formPwd.value['oldPwd'].trim(),
        newPwd: this.formPwd.value['newPwd'].trim()
      };

      this.userService.changePwd(body).pipe(
        take(1),
        finalize(() => {
          this.spinner.hide()
        })
      ).subscribe(() => this.formPwd.reset());
    }
  }

}
