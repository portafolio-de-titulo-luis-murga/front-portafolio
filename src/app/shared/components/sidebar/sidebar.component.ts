import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import PerfectScrollbar from 'perfect-scrollbar';

import { LoginService } from 'src/app/services/api/login.service';
import { RouteInfo } from 'src/app/types/interfaces/route-info.interface';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();
  public menuItems: RouteInfo[];
  ps: any;
  public isMobileMenu: boolean;

  constructor(
    private breakpoint: BreakpointObserver,
    private loginService: LoginService
  ) {
    this.isMobileMenu = this.breakpoint.isMatched('(max-width: 991px)');
  }

  ngOnInit(): void {
    this.isMobileMenuObserver();

    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      this.ps = new PerfectScrollbar(elemSidebar);
    }
    this.menuItems = this.loginService.menuRole();
  }

  logout(): void {
    this.loginService.logOut();
  }

  isMobileMenuObserver(): void {
    const suscript = this.breakpoint.observe(['(max-width: 991px)']).pipe(takeUntil(this.unsubscribe$))
      .subscribe(layout => this.isMobileMenu = layout.breakpoints['(max-width: 991px)']);
    this.subscription.add(suscript);
  }

  updatePS(): void  {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      this.ps.update();
    }
  }

  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
