import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Chartist from 'chartist';

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.css']
})
export class GraphicsComponent implements OnInit {

  public items: any[] = [];

  public colorHeaderItems: any[] = [
    'card-header-warning',
    'card-header-rose',
    'card-header-success',
    'card-header-info',
    'card-header-danger',
    'card-header-primary',
    'card-header-warning'
  ];

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.items = this.activatedRoute.snapshot.data['rslv'];
  }

}
