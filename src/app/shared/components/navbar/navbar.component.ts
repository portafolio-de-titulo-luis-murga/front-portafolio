import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  private subscription: Subscription = new Subscription();
  location: Location;
  nativeElement: Node;
  private toggleButton: any;
  private sidebarVisible: boolean;
  private misc: any = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
  };

  constructor(
    location: Location,
    private element: ElementRef,
    private router: Router,
    private breakpoint: BreakpointObserver
  ) {
    this.location = location;
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
  }

  ngOnInit(): void {
    this.initStyle();
    this.isNotMobile();
  }

  initStyle(): void {
    const navbar: HTMLElement = this.element.nativeElement;
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    if (body.classList.contains('sidebar-mini')) {
      this.misc.sidebar_mini_active = true;
    }
    if (body.classList.contains('hide-sidebar')) {
      this.misc.hide_sidebar_active = true;
    }
    const suscrip = this.router.events.pipe(
      takeUntil(this.unsubscribe$),
      filter(event => event instanceof NavigationEnd)
      ).subscribe((event: NavigationEnd) => {
      this.sidebarClose();

      this.deleteLayerMobile();
    });
    this.subscription.add(suscrip);
  }

  deleteLayerMobile(): void {
    const $layer = document.getElementsByClassName('close-layer')[0];
    if ($layer) {
      $layer.remove();
    }
  }

  isNotMobile(): void {
    const suscrip = this.breakpoint.observe(['(max-width: 991px)']).pipe(
        takeUntil(this.unsubscribe$),
        filter(ly => !ly.breakpoints['(max-width: 991px)'])
      ).subscribe(() => {
        this.sidebarClose()
        this.deleteLayerMobile()
      });
    this.subscription.add(suscrip);
  }

  minimizeSidebar(): void {
    const body = document.getElementsByTagName('body')[0];

    if (this.misc.sidebar_mini_active === true) {
      body.classList.remove('sidebar-mini');
      this.misc.sidebar_mini_active = false;
    } else {
      setTimeout(() => {
        body.classList.add('sidebar-mini');
        this.misc.sidebar_mini_active = true;
      }, 300);
    }
  }

  sidebarOpen(): void {
    const body = document.getElementsByTagName('body')[0];

    setTimeout(() => {
      this.toggleButton.classList.add('toggled');
    }, 500);

    body.classList.add('nav-open');

    let $layer = document.createElement('div');
    $layer.setAttribute('class', 'close-layer');


    if (body.querySelectorAll('.main-panel')) {
      document.getElementsByClassName('main-panel')[0].appendChild($layer);
    }

    setTimeout(() => {
      $layer.classList.add('visible');
    }, 100);

    $layer.onclick = function() {
      body.classList.remove('nav-open');
      this.sidebarVisible = false;

      $layer.classList.remove('visible');
      setTimeout(() => {
        $layer.remove();
        this.toggleButton.classList.remove('toggled');
      }, 400);
    }.bind(this);

    this.sidebarVisible = true;
  }

  sidebarClose(): void {
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton.classList.remove('toggled');
    this.sidebarVisible = false;
    body.classList.remove('nav-open');
  }

  sidebarToggle(): void {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  getTitle(): string {
    const path = this.location.path();
    const sections = path.split('/');
    const baseTitle = sections.slice(0, 3).join('/');

    const mapTitle = {
      '/home': 'Home',
      '/provider': 'Proveedores',
      '/provider/create': 'Crear Proveedor',
      '/provider/update': 'Actualizar Proveedor',
      '/report/anexo-unidad': 'Reporte Anexo/Unidad',
      '/report/responsable': 'Reporte Responsable',
      '/report/general': 'Reporte Universidad',
      '/user': 'Usuarios',
      '/user/create': 'Crear Usuario',
      '/user/update': 'Actualizar Usuario',
      '/unit': 'Unidades',
      '/unit/create': 'Crear Unidad',
      '/unit/update': 'Actualizar Unidad',
      '/responsable/home': 'Home',
      '/responsable/tarificacion': 'Consulta Tarificación',
      '/anexo': 'Anexos',
      '/anexo/create': 'Crear Anexo',
      '/anexo/update': 'Actualizar Anexo',
      '/cambiarClave': 'Cambiar Contraseña',
      '/responsable/cambiarClave': 'Cambiar Contraseña',
      '/responsable/unit': 'Mis Unidades',
    };

    return mapTitle[path] || mapTitle[baseTitle] || 'Página no reconocida';
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.subscription.unsubscribe();
  }

}
