import { TestBed } from '@angular/core/testing';

import { GetUnitsResolver } from './get-units.resolver';

describe('GetUnitsResolver', () => {
  let resolver: GetUnitsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetUnitsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
