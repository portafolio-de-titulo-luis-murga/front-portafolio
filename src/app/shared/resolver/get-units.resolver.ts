import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { UnitService } from 'src/app/services/api/unit.service';

@Injectable({
  providedIn: 'root'
})
export class GetUnitsResolver implements Resolve<boolean> {

  constructor(
    private unitService: UnitService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this.unitService.getUnits().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
