import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { FacultyService } from 'src/app/services/api/faculty.service';

@Injectable({
  providedIn: 'root'
})
export class ResourceCountResolver implements Resolve<boolean> {

  constructor(
    private facultyService: FacultyService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.spinner.show();

    const nameDictionary = {
      countAnexo: {
        name: 'Anexos',
        icon: 'tag'
      },
      countFaculty: {
        name: 'Facultades',
        icon: 'apartment'
      },
      countNumberOfCallAnswered: {
        name: 'Llamadas Contestadas',
        icon: 'tty'
      },
      countProvider: {
        name: 'Proveedores',
        icon: 'cell_tower'
      },
      countSpokenSecond: {
        name: 'Minutos Hablados',
        icon: 'phone_in_talk'
      },
      countStorageCall: {
        name: 'Cant. Llamadas',
        icon: 'grid_on'
      },
      countSupervisor: {
        name: 'Supervisores',
        icon: 'supervisor_account'
      },
      countUnit: {
        name: 'Unidades',
        icon: 'holiday_village'
      }
    };

    return this.facultyService.resourceCount().pipe(
      map(data => {
        let dataMap = [];

        for (let key in data) {
          dataMap.push({ ...nameDictionary[key], count: data[key] });
        }

        return dataMap;
      }),
      finalize(() => this.spinner.hide())
    );
  }
}
