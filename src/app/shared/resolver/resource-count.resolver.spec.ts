import { TestBed } from '@angular/core/testing';

import { ResourceCountResolver } from './resource-count.resolver';

describe('ResourceCountResolver', () => {
  let resolver: ResourceCountResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(ResourceCountResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
