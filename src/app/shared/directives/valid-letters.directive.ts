import { Directive, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appValidLetters]'
})
export class ValidLettersDirective {

  private patterns: {[key: string]: RegExp} = {
    onlyLettersSpace:  /[^a-zA-Z ]+$/,
    onlyLettersSpaceDot:  /[^a-zA-Z. ]+$/,
    lettersMinNumber: /[^a-z0-9]+$/,
    lettersMinMayNumber: /[^a-zA-Z0-9]+$/,
    lettersMinMayNumberSpace: /[^a-zA-Z0-9 ]+$/,
    emailValidLetters: /[^a-z0-9@.]+$/,
    validPwd:  /[^a-zA-Z0-9.*/$]+$/,
    validNumbers:  /[^0-9]+$/,
  };

  @Input('appValidLetters') patternName: string;

  constructor(private ngControl: NgControl) { }

  @HostListener('input', ['$event'])
  onInput(event: KeyboardEvent): void {
    const regex = this.patterns[this.patternName];
    const inputElement = event.target as HTMLInputElement;
    const inputValue = inputElement.value;
    inputElement.value = inputValue.replace(regex, '');
    this.ngControl.control.patchValue(inputValue.replace(regex, ''));
    event.preventDefault();
  }

  @HostListener("keydown", ["$event"])
  onKeyDown(event: KeyboardEvent): void {
    const regex = this.patterns[this.patternName];
    if (regex.test(event.key)) {
      event.preventDefault();
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent): void {
    event.preventDefault();
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent): void {
    event.preventDefault();
  }

}
