import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appOnlyNumber]'
})
export class OnlyNumberDirective {

  constructor(private ngControl: NgControl) { }

  @HostListener("input", ["$event"])
  onInput(event: KeyboardEvent): void {
    const inputElement = event.target as HTMLInputElement;
    let inputValue = inputElement.value;
    inputValue = inputValue.replace(/[^0-9]*/g, '');
    inputElement.value = inputValue;
    this.ngControl.control.patchValue(inputValue);
  }

  @HostListener("keydown", ["$event"])
  onKeyDown(event: KeyboardEvent): void {
    const numericValue = new RegExp('^[0-9]+$');

    // Permitir: backspace, delete, tab, escape, enter y las flechas (flecha izquierda, flecha derecha, flecha arriba, flecha abajo)
    if ([46, 8, 9, 27, 13, 37, 38, 39, 40].indexOf(event.keyCode) !== -1) {
      return;
    }

    if (!numericValue.test(event.key)) {
      event.preventDefault();
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent): void {
    event.preventDefault();
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent): void {
    event.preventDefault();
  }

}
