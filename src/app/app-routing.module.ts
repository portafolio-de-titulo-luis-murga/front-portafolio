import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout.component';
import { ValidSessionGuard } from './guards/valid-session.guard';


export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canLoad: [ValidSessionGuard],
    canActivate: [ValidSessionGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'responsable',
        loadChildren: () => import('./modules/supervisor/supervisor.module').then(m => m.SupervisorModule)
      }
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
