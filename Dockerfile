FROM node:14-slim AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install --no-optional

COPY . .

RUN npm run build

# NGINX
FROM nginx:alpine

COPY --from=builder /app/dist/admin-tarificacion /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
